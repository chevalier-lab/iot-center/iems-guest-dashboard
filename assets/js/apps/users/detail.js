"use strict";

var phone_number = "";
var user = null;

!function (NioApp, $) {
  "use strict";

  phone_number = $("#selected-user-phone_number").val();
  loadDetail();

  $("#btn-user-back").on("click",function() {
    $("#btn-user-back").attr("disabled", true);
    $("#btn-user-back").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    setTimeout(() => {
      window.history.back();
    }, 1000);
  });
}(NioApp, jQuery);

function loadDetail() {
  $("#detail-user").html(`
    <div class="p-2">
      <center>
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
      </center>
    </div>
  `);

  global.getRAW(global.base_url + "/services/users/detail?phone_number=" + phone_number + "&tokenize=" + global.csrf_token, 
    function(res) {

        if (res.code == 200) {
            user = res.data;

            getStatus(user.type, user.status, function(status) {
              $("#detail-user").html(`
          <div class="card-inner">

            <div class="card-title-group align-start pb-3 g-2">
                <div class="card-title card-title-sm">
                    <h6 class="title">Detail ${user.full_name}</h6>
                    <p>Detail data pengguna IEMS</p>
                </div>
                <div class="card-tools">
                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Detail data pengguna IEMS"></em>
                </div>
            </div>
            
            <div class="nk-block">
                <div class="profile-ud-list">
                    <div class="profile-ud-item">
                        <div class="profile-ud wider">
                            <span class="profile-ud-label">Nama Lengkap</span>
                            <span class="profile-ud-value">${user.full_name}</span>
                        </div>
                    </div>
                    <div class="profile-ud-item">
                        <div class="profile-ud wider">
                            <span class="profile-ud-label">Email</span>
                            <span class="profile-ud-value">${user.email}</span>
                        </div>
                    </div>
                    <div class="profile-ud-item">
                        <div class="profile-ud wider">
                            <span class="profile-ud-label">Nomor Telepon</span>
                            <span class="profile-ud-value">${user.phone_number}</span>
                        </div>
                    </div>
                    <div class="profile-ud-item">
                        <div class="profile-ud wider">
                            <span class="profile-ud-label">Status</span>
                            <span class="profile-ud-value">${status}</span>
                        </div>
                    </div>
                </div><!-- .profile-ud-list -->
            </div>

            <hr>

            <div class="card-title-group align-start pb-3 g-2">
                <div class="card-title card-title-sm">
                    <h6 class="title">Informasi Tambahan</h6>
                    <p>Informasi tambahan pengguna IEMS</p>
                </div>
                <div class="card-tools">
                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Additional Information"></em>
                </div>
            </div>

            <div class="nk-block">
                <div class="profile-ud-list">
                    <div class="profile-ud-item">
                        <div class="profile-ud wider">
                            <span class="profile-ud-label">Tanggal Aktif</span>
                            <span class="profile-ud-value">${(user.status == "activate") ? global.date(user.created_at) : "-"}</span>
                        </div>
                    </div>
                    <div class="profile-ud-item">
                        <div class="profile-ud wider">
                            <span class="profile-ud-label">Aksi</span>
                            <span class="profile-ud-value">
                              
                            </span>
                        </div>
                    </div>
                </div><!-- .profile-ud-list -->
            </div>

        </div>
          `);
            });

            setupTabs()
            return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function getStatus(type, status, callback) {
  if (type == "administrator") {
    status = `<span class="badge badge-pill badge-primary">Administrator</span>`;
  }
  else if (type == "operator") {
      status = `<span class="badge badge-pill badge-primary">Operator</span>`;
  }
  else if (type == "customer service") {
      status = `<span class="badge badge-pill badge-primary">CS</span>`;
  }
  else if (type == "user") {
      status = `<span class="badge badge-pill badge-success">Aktif (Pembeli)</span>`;
  }
  else if (type == "guest" && status == "activate") {
      status = `<span class="badge badge-pill badge-info">Terverifikasi</span>`;
  }
  else if (type == "guest" && status == "request activate") {
      status = `<span class="badge badge-pill badge-warning">Permintaan Berlangganan</span>`;
  } else if (type == "guest" && (status == "not activate yet" || status == "banned")) {
      status = `<span class="badge badge-pill badge-warning">Biasa</span>`;
  }

  callback(status);
}

function setupTabs() {
  var tabs = ``;
  var type = user.type;

  if (type == "operator" || type == "customer service" || type == "administrator") {
    tabs += `
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="javascript:void(0)"
    onclick="loadMyInstalationUser()">Daftar Pelanggan</a>
  </li>
  `;
  loadMyInstalationUser()
  }
  else if (type == "customer service") {
    tabs += `
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="javascript:void(0)"
    onclick="loadMyInstalationUser()">Daftar Pelanggan</a>
  </li>
  `;
  loadMyInstalationUser()
  }
  else if (type == "user" && user.status == "activate") {
    tabs += `
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="javascript:void(0)"
    onclick="loadMyDevices('${user.phone_number}')">Daftar Perangkat</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="javascript:void(0)"
    onclick="loadGuestByUser('${user.phone_number}')">Daftar Pengguna Tambahan</a>
  </li>
  `;
  loadMyDevices(user.phone_number)
  }
  else if (type == "guest" && user.status == "activate") {
    tabs += `
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="javascript:void(0)"
    onclick="loadMyDevices('${user.phone_number}')">Daftar Perangkat</a>
  </li>
  `;
  loadMyDevices(user.phone_number)
  }
  else if (type == "guest" && user.status != "activate") tabs += ``;

  $("#detail-user-list-tabs").html(tabs);
}

function loadGuestByUser(phone_number)
{
  $("#tab-content").html(`
    <div class="p-2">
      <center>
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
      </center>
    </div>
  `);

  global.getRAW(global.base_url + "/services/users/listGuestByUser?phone_number=" + phone_number + "&tokenize=" + global.csrf_token, 
    function(res) {

        if (res.code == 200) {
            setupGuestByUser(res.data);
            return
        }

        $("#tab-content").html(`
          <div class="p-2">
            <center>
              Belum ada pengguna tambahan.
            </center>
          </div>
        `);

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function setupGuestByUser(data) {
  $("#tab-content").html(`
  <div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Lengkap</th>
                <th scope="col">Nomor Telepon</th>
                <th scope="col">E-Mail</th>
                <th scope="col">Status</th>
                <th scope="col">Tanggal Aktif</th>
            </tr>
        </thead>
        <tbody>
    ${data.map(function(item, position) {
        var status = ``;

        if (item.type == "administrator") {
            status = `<span class="badge badge-pill badge-primary">Administrator</span>`;
        }
        else if (item.type == "operator") {
            status = `<span class="badge badge-pill badge-primary">Operator</span>`;
        }
        else if (item.type == "customer service") {
            status = `<span class="badge badge-pill badge-primary">CS</span>`;
        }
        else if (item.type == "user" && item.status == "activate") {
            status = `<span class="badge badge-pill badge-success">Aktif (Pembeli)</span>`;
        }
        else if (item.type == "user" && item.status != "activate") {
            status = `<span class="badge badge-pill badge-danger">Tidak Aktif (Pembeli)</span>`;
        }
        else if (item.type == "guest" && item.status == "activate") {
            status = `<span class="badge badge-pill badge-info">Terverifikasi</span>`;
        }
        else if (item.type == "guest" && item.status != "activate") {
            status = `<span class="badge badge-pill badge-warning">Biasa</span>`;
        }

        return `
        <tr>
            <th scope="row">${(position + 1)}</th>
            <td>${item.full_name}</td>
            <td>${item.phone_number}</td>
            <td>${item.email}</td>
            <td>${status}</td>
            <td>${(item.status == "activate") ? global.date(item.created_at) : "-"}</td>
        </tr>
        `;
    }).join('')}
    
    </tbody>
    </table>
</div>
`);
return
}

function loadMyDevices(phone_number)
{
  $("#tab-content").html(`
    <div class="p-2">
      <center>
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
      </center>
    </div>
  `);

  global.getRAW(global.base_url + "/services/devices/userOrGuestDevice?phone_number=" + phone_number + "&tokenize=" + global.csrf_token, 
    function(res) {

        if (res.code == 200) {
            if (res.data.length == 0) {
              $("#tab-content").html(`
                <div class="p-2">
                  <center>
                    Belum ada perangkat yang terpasang.
                  </center>
                </div>
              `);
            } else 
            setupMyDevices(res.data);
            return
        }

        $("#tab-content").html(`
          <div class="p-2">
            <center>
              Belum ada perangkat yang terpasang.
            </center>
          </div>
        `);

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function setupMyDevices(data)
{
  $("#tab-content").html(`

  <div class="table-responsive">
      <table class="table" id="list-devices">
          <thead>
              <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nama Perangkat</th>
                  <th scope="col">ID Perangkat</th>
                  <th scope="col">Jenis Perangkat</th>
                  <th scope="col">Status</th>
                  <th scope="col">Tanggal Aktif</th>
                  <th scope="col">Aksi</th>
              </tr>
          </thead>
          <tbody>

  ${data.map(function(item, position) {
    var type = getStatusDevice(item.device_type);
    var status = getStatusDevice(item.device_status);
    var action = `
    <button class="btn btn-icon btn-sm btn-primary mb-1"
    onclick="gotoDetailDevice()">
    <em class="icon ni ni-eye"></em></button>
    `;

    if (item.device_status == "not active yet") {
      action += `
      <button class="btn btn-icon btn-sm btn-success mb-1"
      data-toggle="modal" data-target="#modalActivate">
      <em class="icon ni ni-check"></em></button>
      
      <button class="btn btn-icon btn-sm btn-danger mb-1"
      data-toggle="modal" data-target="#modalDelete">
      <em class="icon ni ni-trash"></em></button>`
    } else if (item.device_status == "blocked") {
      action += `
      <button class="btn btn-icon btn-sm btn-success mb-1"
      data-toggle="modal" data-target="#modalEnabled">
      <em class="icon ni ni-check"></em></button>`
    } else {
      action += `
      <button class="btn btn-icon btn-sm btn-danger mb-1"
      data-toggle="modal" data-target="#modalBanned">
      <em class="icon ni ni-cross"></em></button>`
    }

    return `
    <tr>
      <th scope="row">${(position + 1)}</th>
      <td>
          ${item.device_name}<br>
          <small>${item.device_shortname}</small>
      </td>
      <td>
          <small>${item.device_token}</small>
      </td>
      <td>${type}</td>
      <td>${status}</td>
      <td>${(item.device_status == "not active yet") ? "-" : global.date(item.updated_at)}</td>
      <td>${action}</td>
  </tr>
    `;
}).join('')}

</tbody>
</table>
</div>
`);
}

function getStatusDevice(status) {
  var newStatus = `<span class="badge badge-pill badge-success">Aktif</span>`
  switch (status) {
      case "kwh-1-phase": newStatus = `<span class="badge badge-pill badge-outline-warning">KWH 1 Fasa</span>`;
      break;
      case "kwh-3-phase": newStatus = `<span class="badge badge-pill badge-outline-info">KWH 3 Fasa</span>`;
      break;
      case "pcb": newStatus = `<span class="badge badge-pill badge-outline-primary">PCB</span>`;
      break;
      case "sensors": newStatus = `<span class="badge badge-pill badge-outline-success">Sensor</span>`;
      break;
      case "slca": newStatus = `<span class="badge badge-pill badge-outline-secondary">SLCA</span>`;
      break;
      case "active": newStatus = `<span class="badge badge-pill badge-success">Aktif</span>`
      break;
      case "not active yet": newStatus = `<span class="badge badge-pill badge-secondary">Belum Diaktifasi</span>`
      break;
      case "blocked": newStatus = `<span class="badge badge-pill badge-danger">Terblokir</span>`
      break;
  }
  return newStatus;
}

function loadMyInstalationUser()
{
  $("#tab-content").html(`
    <div class="p-2">
      <center>
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
      </center>
    </div>
  `);

  global.getRAW(global.base_url + "/services/users/my?tokenize=" + global.csrf_token, 
    function(res) {

        if (res.code == 200) {
            setupInstalationUser(res.data);
            return
        }

        $("#tab-content").html(`
          <div class="p-2">
            <center>
              Belum ada pelanggan yang instalasi.
            </center>
          </div>
        `);

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function setupInstalationUser(data) {
  $("#tab-content").html(`
  <div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Lengkap</th>
                <th scope="col">Nomor Telepon</th>
                <th scope="col">Status</th>
                <th scope="col">Tanggal Aktif</th>
            </tr>
        </thead>
        <tbody>
    ${data.map(function(item, position) {
        var status = ``;

        if (item.type == "administrator") {
            status = `<span class="badge badge-pill badge-primary">Administrator</span>`;
        }
        else if (item.type == "operator") {
            status = `<span class="badge badge-pill badge-primary">Operator</span>`;
        }
        else if (item.type == "customer service") {
            status = `<span class="badge badge-pill badge-primary">CS</span>`;
        }
        else if (item.type == "user" && item.status == "activate") {
            status = `<span class="badge badge-pill badge-success">Aktif (Pembeli)</span>`;
        }
        else if (item.type == "user" && item.status != "activate") {
            status = `<span class="badge badge-pill badge-danger">Tidak Aktif (Pembeli)</span>`;
        }
        else if (item.type == "guest" && item.status == "activate") {
            status = `<span class="badge badge-pill badge-info">Terverifikasi</span>`;
        }
        else if (item.type == "guest" && item.status != "activate") {
            status = `<span class="badge badge-pill badge-warning">Biasa</span>`;
        }

        return `
        <tr>
            <th scope="row">${(position + 1)}</th>
            <td>${item.full_name}</td>
            <td>${item.phone_number}</td>
            <td>${status}</td>
            <td>${(item.status == "activate") ? global.date(item.created_at) : "-"}</td>
        </tr>
        `;
    }).join('')}
    
    </tbody>
    </table>
</div>
`);
return
}