"use strict";

var users = []
var user = null;

!function (NioApp, $) {
  "use strict";

  // Load Filter Users
  filterUsers()

  $("#btn-user-invited").on("click",function() {
    $("#btn-user-invited").attr("disabled", true);
    $("#btn-user-invited").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doInvitedUser();
  });

}(NioApp, jQuery);

function gotoDetailUser(position) {
    user = users[position];
    location.assign(global.base_url + "/views/usersDetail?tokenize=" + global.csrf_token + "&uid=" + user.phone_number + "&uname=" + user.full_name);
}

function doInvitedUser() {
  var phone_number = $("#user-new-phone_number").val();

  var raw = global.raw({
    phone_number: phone_number
  })

  var formData = new FormData();
  formData.append("raw", raw);

  // Do Login
  global.postRAW(formData, 
    global.base_url + "/services/users/invited?tokenize=" + global.csrf_token, function(res) {
      
      $("#btn-user-invited").removeAttr("disabled");
      $("#btn-user-invited").html(`
        <span>Undang</span>
        <em class="icon ni ni-arrow-right"></em>
      `);

      if (res.code == 200) {

        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
        $("#modal-invited-user-form").trigger("reset");
        $("#modal-invited-user").modal('toggle');

        return
      }
      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function filterUsers()
{
    $("#user-list tbody").html(`
        <tr>
            <td scope="col" colspan="6">
                <center>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span> Loading... </span>
                </center>
            </td>
        </tr>
    `);

    global.getRAW(global.base_url + "/services/users/filter?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
            users = res.data;

            if (users.length == 0) {
                $("#user-list tbody").html(`
                    
                    <tr>
                        <td colspan="6">
                            <center>Belum ada pengguna</center>
                        </td>
                    </tr>
                        
                `);
                return
            }

            $("#user-list tbody").html(`
                ${res.data.map(function(item, position) {
                    var status = ``;
                    var action = ``;

                    if (item.type == "administrator") {
                        status = `<span class="badge badge-pill badge-primary">Administrator</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})"><em class="icon ni ni-eye"></em></button>
                        `;
                    }
                    else if (item.type == "operator") {
                        status = `<span class="badge badge-pill badge-primary">Operator</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})"><em class="icon ni ni-eye"></em></button>
                        `;
                    }
                    else if (item.type == "customer service") {
                        status = `<span class="badge badge-pill badge-primary">CS</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})"><em class="icon ni ni-eye"></em></button>
                        `;
                    }
                    else if (item.type == "user") {
                        status = `<span class="badge badge-pill badge-success">Aktif (Pembeli)</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})"><em class="icon ni ni-eye"></em></button>
                        `;

                        if (item.status == "activate") {
                            status = `<span class="badge badge-pill badge-success">Aktif (Pembeli)</span>`;
                        } else if (item.status == "banned") {
                            status = `<span class="badge badge-pill badge-danger">Tidak Aktif (Pembeli)</span>`;
                        }
                    }
                    else if (item.type == "guest" && item.status == "activate") {
                        status = `<span class="badge badge-pill badge-info">Terverifikasi</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})"><em class="icon ni ni-eye"></em></button>
                        `;
                    }
                    else if (item.type == "guest" && item.status == "request activate") {
                        status = `<span class="badge badge-pill badge-warning">Permintaan Berlangganan</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})"><em class="icon ni ni-eye"></em></button>
                        `;
                    } else if (item.type == "guest" && (item.status == "not activate yet" || item.status == "banned")) {
                        status = `<span class="badge badge-pill badge-warning">Biasa</span>`;
                        action = `
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="gotoDetailUser(${position})"><em class="icon ni ni-eye"></em></button>
                        `;
                    }

                    return `
                    <tr>
                        <th scope="row">${(position + 1)}</th>
                        <td>${item.full_name}</td>
                        <td>${item.phone_number}</td>
                        <td>${status}</td>
                        <td>${(item.status == "activate") ? global.date(item.created_at) : "-"}</td>
                        <td>${action}</td>
                    </tr>
                    `;
                }).join('')}
            `);
            return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}