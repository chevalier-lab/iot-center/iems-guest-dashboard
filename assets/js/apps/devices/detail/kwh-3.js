"use strict";

var device_token;
var device = null;
var dataDevices = [];
var metaDataDevices = null;
var page = 0;
var selectedDiagramType = null;
var selectedParam = null;

var deviceParams = [
  {
    "id": "e",
    "name": "Energi",
    "action": "last",
    "default": 1,
    "params": ["ep"]
  },
  {
    "id": "p",
    "name": "Daya Aktif",
    "action": "last",
    "default": 0,
    "params": ["pa", "pb", "pc", "pt"]
  },
  {
    "id": "q",
    "name": "Daya Reaktif",
    "action": "last",
    "default": 0,
    "params": ["qa", "qb", "qc", "qt"]
  },
  {
    "id": "s",
    "name": "Daya Semu",
    "action": "last",
    "default": 0,
    "params": ["sa", "sb", "sc", "st"]
  },
  {
    "id": "pf",
    "name": "Power Factor",
    "action": "last",
    "default": 0,
    "params": ["pfa", "pfb", "pfc"]
  }
];

!function (NioApp, $) {
  "use strict";

  device_token = $("#selected-device-device_token").val();

  setupParams();

  getInfo();

  getPull();

  $("#btn-device-back").on("click",function() {
    $("#btn-device-back").attr("disabled", true);
    $("#btn-device-back").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    setTimeout(() => {
        window.history.back();
    }, 1000);
  });

  $("#doPlotFilter").on("click",function() {
    getPlot();
  });

}(NioApp, jQuery);

function setupParams() {
  $("#device-plot-param").html(`${deviceParams.map(function(item, position) {
    if (position == 0) {
      return `<option value="${item.id}" selected>${item.name}</option>`;
    } else 
    return `<option value="${item.id}">${item.name}</option>`;
  }).join('')}`);

  setSelectedParam($("#device-plot-param").val())
}

function setSelectedParam(paramID) {
  var index = -1;
  deviceParams.forEach(function(item, position) {
    if (item.id == paramID) index = position;
  })

  if (index > -1) {
    selectedParam = deviceParams[index];
    $("#device-plot-display").val(selectedParam.action);
    $("#device-plot-type").val(selectedParam.default)
    getPlot();
  } else console.log("Not Found")
}

function gotoEditUser() { location.assign(base_url + "/views/usersEdit/1"); }

function setPage(action) {
  if (metaDataDevices != null) {
    var maxPage = Number(metaDataDevices.total_data) / metaDataDevices.total_fetch;
    if ((page + action) < 0) {
      NioApp.Toast("Sudah mencapai minimum", 'error', {position: 'bottom-right'});
    }
    else if ((page + action) < maxPage) {
      page = page + action;
      getPull();
    } 
    else {
      NioApp.Toast("Sudah mencapai maksimal", 'error', {position: 'bottom-right'});
    }
  }
}

function setType(action) {
  if (action == 3 || action == "3") {
    $("#device-plot-date").removeAttr("disabled");
    $("#device-plot-time").removeAttr("disabled");
  } else if (action == 1 || action == "1") {
    $("#device-plot-date").removeAttr("disabled");
    $("#device-plot-time").attr("disabled", true);
  } 
  else {
    $("#device-plot-date").attr("disabled", true);
    $("#device-plot-time").attr("disabled", true);
  }
}

function getPlot()
{
  var action = $("#device-plot-type").val();
  if (action == 0 || action == "0") {
    getPlotDay(global.currDate(), global.currTime());
  } else if (action == 3 || action == "3") {
    var date = $("#device-plot-date").val();
    var time = $("#device-plot-time").val();
    // console.log();
    getPlotDay(date, (time != "") ? global.convTime(time) : "")
  } else if (action == 1 || action == "1") {
    var date = $("#device-plot-date").val();
    if (date == "") date = global.currDate();
    getPlotMonth(date);
  } else if (action == 2 || action == "2") {
    var date = $("#device-plot-date").val();
    if (date == "") date = global.currDate();
    getPlotYear(date);
  }
}

function getPlotDay(date, time)
{
  var param = $("#device-plot-param").val();
  global.getRAW(global.base_url + "/services/dataDevice/day?type=KWH3&param="+param+"&time="+time+"&date="+date+"&device_token=" + device_token + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {

          var data = res.data;
          var meta = res.meta;
          var colors = [];
          var colorDisabled = "rgb(204,204,204)";
          var colorEnabled = "rgb(26, 118, 255)"

          if (selectedParam.id == "e") {
            // Find Anomaly
            var anomaly = false;
            var temp = null;
            var itemsTemp = [];
            var tempColor = [];
            data.forEach(function(item) {
              if (temp == null) {
                if (item.last.ep == "0.00" || item.last.ep == "0") {
                  if (item.first.ep != "0.00") item.last.ep = item.first.ep;
                  else anomaly = true;
                  tempColor.push(colorDisabled);
                } else 
                tempColor.push(colorEnabled);
                temp = item;
              } else {
                if (item.last.ep == "0.00" || item.last.ep == "0") {
                  item.last.ep = temp.last.ep
                  tempColor.push(colorDisabled);
                }
                else 
                tempColor.push(colorEnabled);
                temp = item;
              }
              itemsTemp.push(item);
            })
            colors.push(tempColor);
            data = itemsTemp;

            // Calculate Distance
            var distance = data.slice(1).map(function(v, i) {
              var temp  = data[i];
              temp.last.ep = (Number(v.last.ep) - Number(temp.last.ep)).toString();
              return temp.last.ep
            });

            distance.unshift ("0.00");

            // console.log(distance)

            var dataFix = [];
            var checker = (Number(data[0].first.ep) - Number(data[0].first_day_of_month.ep)) < 0 ? (Number(data[0].first.ep) - Number(data[0].first_day_of_month.ep)) * -1 : (Number(data[0].first.ep) - Number(data[0].first_day_of_month.ep)) 
            var firstValue = checker.toString();
            distance.forEach(function(item) {
              firstValue = (Number(firstValue) + Number(item)).toString();
              dataFix.push(firstValue);
            })

            itemsTemp = data.map(function(item, position) {
              var temp = item;
              temp.last.ep = dataFix[position]
              return temp;
            });

            data = itemsTemp;
          } else {
            var listParam = getParam(param)
            listParam.forEach(function(item) {
              colors.push(random_rgba())
            });
          }

          setupPlotDay(data, meta, colors)

          return;
        } 
    });
}

function getPlotMonth(date)
{
  var param = $("#device-plot-param").val();
  global.getRAW(global.base_url + "/services/dataDevice/month?type=KWH3&param="+param+"&date="+date+"&device_token=" + device_token + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {

          var data = res.data;
          var meta = res.meta;
          var colors = [];
          var colorDisabled = "rgb(204,204,204)";
          var colorEnabled = "rgb(26, 118, 255)"

          if (selectedParam.id == "e") {
            // Find Anomaly
            var anomaly = false;
            var temp = null;
            var itemsTemp = [];
            var tempColor = [];
            data.forEach(function(item) {
              // console.log(item['last']['ep'])
              if (temp == null) {
                if (item.last.ep == "0.00" || item.last.ep == "0") {
                  if (item.first.ep != "0.00" && item.first.ep != "0") item.last.ep = item.first.ep;
                  else anomaly = true;
                  tempColor.push(colorDisabled);
                } else  {
                  if (Number(item.first.ep) > Number(item.last.ep)) item.last.ep = (Number(item.last.ep) + Number(item.first.ep)).toString();
                  tempColor.push(colorEnabled);
                }
                temp = item;
              } else {
                if (item.last.ep == "0.00" || item.last.ep == "0") {
                  item.last.ep = temp.last.ep
                  tempColor.push(colorDisabled);
                }
                else  {
                  if (Number(temp.last.ep) > Number(item.last.ep)) item.last.ep = (Number(temp.last.ep)).toString();
                  tempColor.push(colorEnabled);
                }
                temp = item;
              }
              itemsTemp.push(item);
            })
            colors.push(tempColor);
            data = itemsTemp;
            
            // Calculate Distance
            var distance = data.slice(1).map(function(v, i) {
              var temp  = data[i];
              temp.last.ep = (Number(v.last.ep) - Number(temp.last.ep)).toString();
              return temp.last.ep
            });

            distance.unshift ("0.00");

            var dataFix = [];
            var firstValue = "0.00";

            if (data[0].first_detail.length > 0) {
              var tempF = data[0].first_detail[0].ep
              var tempL = data[0].first_detail[(data[0].first_detail.length -1)].ep
              var checker = (Number(tempL) - Number(tempF)) < 0 ? (Number(tempL) - Number(tempF)) * -1 : (Number(tempL) - Number(tempF)) 
              firstValue = checker.toString();
            }

            distance.forEach(function(item) {
              firstValue = (Number(firstValue) + Number(item)).toString();
              dataFix.push(firstValue);
            })

            // console.log({
            //   data: data,
            //   distance: distance,
            //   fix: dataFix
            // })

            itemsTemp = data.map(function(item, position) {
              var temp = item;
              temp.last.ep = dataFix[position]
              return temp;
            });

            data = itemsTemp;
          } else {
            var listParam = getParam(param)
            listParam.forEach(function(item) {
              colors.push(random_rgba())
            });
          }

          setupPlotMonth(data, meta, colors)

          return;
        } 
    });
}

function getPlotYear(date)
{
  var param = $("#device-plot-param").val();
  global.getRAW(global.base_url + "/services/dataDevice/year?type=KWH3&param="+param+"&date="+date+"&device_token=" + device_token + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {

          var data = res.data;
          var meta = res.meta;
          var colors = [];
          var colorEnabled = "rgb(26, 118, 255)"

          if (selectedParam.id == "e") {
            var tempColor = [];
            var itemsTemp = data.map(function(item, position) {
              var temp = item;
              tempColor.push(colorEnabled);
              var checker = (Number(item.last.ep) - Number(item.first.ep)) < 0 ? (Number(item.last.ep) - Number(item.first.ep)) * -1 : (Number(item.last.ep) - Number(item.first.ep)) 
              temp.last.ep = checker.toString();
              return temp;
            });
            colors.push(tempColor)
            data = itemsTemp;
          } else {
            var listParam = getParam(param)
            listParam.forEach(function(item) {
              colors.push(random_rgba())
            });
          }

          setupPlotYear(data, meta, colors)

          return;
        } 
    });
}

function random_rgba() {
  var o = Math.round, r = Math.random, s = 255;
  return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
}

function getParam(param) {
  var listParams = []
  deviceParams.forEach(function(item) {
    if (item.id == param) listParams = item.params;
  });
  return listParams;
}

function displayParam(param) {
  switch (param) {
    case "v": return "Voltage";
    case "i": return "Current"
    case "p": return "Daya Aktif"
    case "ap": return "Apparent Power"
    case "pf": return "Power Factor"
    case "freq": return "Frequency"
    case "q": return "Daya Reaktif"
    case "s": return "Daya Semu"
    default: return "Energi";
  }
}

function displayParamUnit(param) {
  switch (param) {
    case "v": return "Voltage";
    case "i": return "Current"
    case "p": return "Watt"
    case "ap": return "Apparent Power"
    case "pf": return "Power Factor"
    case "freq": return "Frequency"
    case "q": return "VAR"
    case "s": return "VA"
    default: return "kWH";
  }
}

function getSUM(param, item) {
  return Number(item['sum'][param])
}

function getAVG(param, item) {
  return Number(item['avg'][param])
}

function getLAST(param, item) {
  return Number(item['last'][param])
}

function setupPlotDay(data, meta, colors)
{
  var display = $("#device-plot-display").val();
  var param = $("#device-plot-param").val();
  var diagram = [];
  
  var listParam = getParam(param)
  listParam.forEach(function(itemParam, index) {
    var x = data.map(function(item) {
      return item.time;
    })
    var y = data.map(function(item) {
      if (display == "sum")
      return getSUM(itemParam, item)
      else if (display == "avg")
      return getAVG(itemParam, item)
      else if (display == "last")
      return getLAST(itemParam, item)
    })
    var text = data.map(function(item) {
      if (display == "sum")
      return "Jumlah "+displayParam(param)+" pada jam " + item.time + ", adalah " + getSUM(itemParam, item)
      if (display == "avg")
      return "Rata-rata "+displayParam(param)+" pada jam " + item.time + ", adalah " + getAVG(itemParam, item)
      if (display == "last")
      return "Data terakhir "+displayParam(param)+" pada jam " + item.time + ", adalah " + getLAST(itemParam, item)
    })
    var trace1 = {
      // x: ['Liam', 'Sophie', 'Jacob', 'Mia', 'William', 'Olivia'],
      x: x,
      // y: [8.0, 8.0, 12.0, 12.0, 13.0, 20.0],
      y: y,
      marker:{
        color: colors[index]
      },
      type: 'bar',
      // text: ['4.17 below the mean', '4.17 below the mean', '0.17 below the mean', '0.17 below the mean', '0.83 above the mean', '7.83 above the mean'],
      text: text
    };

    diagram.push(trace1)
  });
  
  var layout = {
    title: `Data ${displayParam(param)} pada tanggal ${meta.last_date}`,
    font:{
      family: 'Raleway, sans-serif'
    },
    showlegend: false,
    xaxis: {
      tickangle: -45,
      title: 'Waktu (Jam)'
    },
    yaxis: {
      zeroline: false,
      gridwidth: 2,
      title: displayParam(param) + "("+displayParamUnit(param)+")"
    }
  };
  
  Plotly.newPlot('display-plot', diagram, layout);  
}

function setupPlotMonth(data, meta, colors)
{
  var display = $("#device-plot-display").val();
  var param = $("#device-plot-param").val();
  var diagram = [];
  
  var listParam = getParam(param)
  listParam.forEach(function(itemParam, index) {
    var x = data.map(function(item) {
      return item.date;
    })
    var y = data.map(function(item) {
      if (display == "sum")
      return getSUM(itemParam, item)
      else if (display == "avg")
      return getAVG(itemParam, item)
      else if (display == "last")
      return getLAST(itemParam, item)
    })
  
    var text = data.map(function(item) {
      if (display == "sum")
      return "Jumlah "+displayParam(param)+" pada tanggal " + item.date + ", adalah " + getSUM(itemParam, item)
      if (display == "avg")
      return "Rata-rata "+displayParam(param)+" pada tanggal " + item.date + ", adalah " + getAVG(itemParam, item)
      if (display == "last")
      return "Data terakhir "+displayParam(param)+" pada tanggal " + item.date + ", adalah " + getLAST(itemParam, item)
    })
    var trace1 = {
      // x: ['Liam', 'Sophie', 'Jacob', 'Mia', 'William', 'Olivia'],
      x: x,
      // y: [8.0, 8.0, 12.0, 12.0, 13.0, 20.0],
      y: y,
      marker:{
        color: colors[index]
      },
      type: 'bar',
      // text: ['4.17 below the mean', '4.17 below the mean', '0.17 below the mean', '0.17 below the mean', '0.83 above the mean', '7.83 above the mean'],
      text: text
    };

    diagram.push(trace1);
  });

  var layout = {
    title: `Data ${displayParam(param)} pada tanggal ${meta.first_date} s/d ${meta.last_date}`,
    font:{
      family: 'Raleway, sans-serif'
    },
    showlegend: false,
    xaxis: {
      tickangle: -45,
      title: 'Tanggal'
    },
    yaxis: {
      zeroline: false,
      gridwidth: 2,
      title: displayParam(param) + "("+displayParamUnit(param)+")"
    }
  };
  
  Plotly.newPlot('display-plot', diagram, layout);  
}

function setupPlotYear(data, meta, colors)
{
  var display = $("#device-plot-display").val();
  var param = $("#device-plot-param").val();
  var diagram = [];
  
  var listParam = getParam(param)
  listParam.forEach(function(itemParam, index) {
    var x = data.map(function(item) {
      return global.month(item.date);
    })
    var y = data.map(function(item) {
      if (display == "sum")
      return getSUM(itemParam, item)
      else if (display == "avg")
      return getAVG(itemParam, item)
      else if (display == "last")
      return getLAST(itemParam, item)
    })
    var text = data.map(function(item) {
      if (display == "sum")
      return "Jumlah "+displayParam(param)+" pada bulan " + global.month(item.date) + ", adalah " + getSUM(itemParam, item)
      if (display == "avg")
      return "Rata-rata "+displayParam(param)+" pada bulan " + global.month(item.date) + ", adalah " + getAVG(itemParam, item)
      if (display == "last")
      return "Data terakhir "+displayParam(param)+" pada bulan " + global.month(item.date) + ", adalah " + getLAST(itemParam, item)
    })
    var trace1 = {
      // x: ['Liam', 'Sophie', 'Jacob', 'Mia', 'William', 'Olivia'],
      x: x,
      // y: [8.0, 8.0, 12.0, 12.0, 13.0, 20.0],
      y: y,
      marker:{
        color: colors[index]
      },
      type: 'bar',
      // text: ['4.17 below the mean', '4.17 below the mean', '0.17 below the mean', '0.17 below the mean', '0.83 above the mean', '7.83 above the mean'],
      text: text
    };
    diagram.push(trace1)
  });
  
  var layout = {
    title: `Data ${displayParam(param)} pada bulan ${global.month(meta.first_date)} s/d ${global.month(meta.last_date)}`,
    font:{
      family: 'Raleway, sans-serif'
    },
    showlegend: false,
    xaxis: {
      tickangle: -45,
      title: 'Bulan'
    },
    yaxis: {
      zeroline: false,
      gridwidth: 2,
      title: displayParam(param) + "("+displayParamUnit(param)+")"
    }
  };
  
  Plotly.newPlot('display-plot', diagram, layout);  
}

function getInfo() {
  $("#detail-device").html(`
      <center>
      <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
      <span> Loading... </span>
      </center>
    `);

    global.getRAW(global.base_url + "/services/devices/info?device_token=" + device_token + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {

          device = res.data;
          
          $("#detail-device").html(`
          <div class="card-title-group align-start pb-3 g-2">
              <div class="card-title card-title-sm">
                  <h6 class="title">Detail data perangkat IEMS</h6>
              </div>
          </div>
          
          <div class="nk-block">
              <div class="profile-ud-list">
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Nama Perangkat</span>
                          <span class="profile-ud-value">
                              ${device.device_name}<br>
                              <small>${device.device_shortname}</small>
                          </span>
                      </div>
                  </div>
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">ID Perangkat</span>
                          <span class="profile-ud-value">${device.device_token}</span>
                      </div>
                  </div>
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Kode Aktifasi</span>
                          <span class="profile-ud-value">20201008201010-8871</span>
                      </div>
                  </div>
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Jenis Perangkat</span>
                          <span class="profile-ud-value">${getStatus(device.device_type)}</span>
                      </div>
                  </div>
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Status</span>
                          <span class="profile-ud-value">${getStatus(device.device_status)}</span>
                      </div>
                  </div>
              </div><!-- .profile-ud-list -->
          </div>

          <hr>

          <div class="card-title-group align-start pb-3 g-2">
              <div class="card-title card-title-sm">
                  <h6 class="title">Additional Information</h6>
                  <p>Informasi tambahan perangkat IEMS</p>
              </div>
              <div class="card-tools">
                  <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Additional Information"></em>
              </div>
          </div>

          <div class="nk-block">
              <div class="profile-ud-list">
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Tanggal Aktif</span>
                          <span class="profile-ud-value">${(device.device_status != "active") ? "-" : global.date(device.created_at)}</span>
                      </div>
                  </div>
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Tanggal Ubah</span>
                          <span class="profile-ud-value">${(device.device_status != "active") ? "-" : global.date(device.updated_at)}</span>
                      </div>
                  </div>
              </div><!-- .profile-ud-list -->
          </div>
          `);

            return;
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function getStatus(status) {
  var newStatus = `<span class="badge badge-pill badge-success">Aktif</span>`
  switch (status) {
      case "kwh-1-phase": newStatus = `<span class="badge badge-pill badge-outline-warning">KWH 1 Fasa</span>`;
      break;
      case "kwh-3-phase": newStatus = `<span class="badge badge-pill badge-outline-info">KWH 3 Fasa</span>`;
      break;
      case "pcb": newStatus = `<span class="badge badge-pill badge-outline-primary">PCB</span>`;
      break;
      case "sensors": newStatus = `<span class="badge badge-pill badge-outline-success">Sensor</span>`;
      break;
      case "slca": newStatus = `<span class="badge badge-pill badge-outline-secondary">SLCA</span>`;
      break;
      case "active": newStatus = `<span class="badge badge-pill badge-success">Aktif</span>`
      break;
      case "not active yet": newStatus = `<span class="badge badge-pill badge-secondary">Belum Diaktifasi</span>`
      break;
      case "blocked": newStatus = `<span class="badge badge-pill badge-danger">Terblokir</span>`
      break;
  }
  return newStatus;
}

function getPull()
{
  $("#list-data-device tbody").html(`
    <tr>
      <td colspan="30">
        <center>
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
        </center>
      </td>
    </tr>
    `);

    global.getRAW(global.base_url + "/services/devices/pull?device_token=" + device_token + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {

          dataDevices = res.data;
          metaDataDevices = res.meta;
          
          $("#list-data-device tbody").html(`${dataDevices.map(function(item, position) {
            return `
            <tr>
                <th scope="row">${(position + 1)}</th>
                <td>${item.va}</td>
                <td>${item.vb}</td>
                <td>${item.vc}</td>
                <td>${item.vab}</td>
                <td>${item.vbc}</td>
                <td>${item.vca}</td>
                <td>${item.ia}</td>
                <td>${item.ib}</td>
                <td>${item.ic}</td>
                <td>${item.pa}</td>
                <td>${item.pb}</td>
                <td>${item.pc}</td>
                <td>${item.pt}</td>
                <td>${item.qa}</td>
                <td>${item.qb}</td>
                <td>${item.qc}</td>
                <td>${item.qt}</td>
                <td>${item.sa}</td>
                <td>${item.sb}</td>
                <td>${item.sc}</td>
                <td>${item.st}</td>
                <td>${item.pfa}</td>
                <td>${item.pfb}</td>
                <td>${item.pfc}</td>
                <td>${item.freq}</td>
                <td>${item.ep}</td>
                <td>${item.eq}</td>
                <td>${item.date}</td>
                <td>${item.time}</td>
            </tr>
            `;
          }).join('')}`);

          if (dataDevices.length == 0) {
          $("#list-data-device tbody").html(`
          <tr>
            <td colspan="30">
              <center>
                Belum ada data perangkat
              </center>
            </td>
          </tr>
          `);
          }

            return;
        } else {
          $("#list-data-device tbody").html(`
            <tr>
              <td colspan="30">
                <center>
                  ${res.message}
                </center>
              </td>
            </tr>
            `);
        }
    });
}