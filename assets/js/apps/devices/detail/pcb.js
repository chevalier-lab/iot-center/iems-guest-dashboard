"use strict";

var device_token;
var device = null;
var dataDevices = [];
var metaDataDevices = null;

!function (NioApp, $) {
  "use strict";

  device_token = $("#selected-device-device_token").val();

  getInfo();

  getPull();

  $("#btn-device-back").on("click",function() {
    $("#btn-device-back").attr("disabled", true);
    $("#btn-device-back").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    setTimeout(() => {
        window.history.back();
    }, 1000);
  });
}(NioApp, jQuery);

function gotoEditUser() { location.assign(base_url + "/views/usersEdit/1"); }

function getInfo() {
  $("#detail-device").html(`
      <center>
      <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
      <span> Loading... </span>
      </center>
    `);

    global.getRAW(global.base_url + "/services/devices/info?device_token=" + device_token + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {

          device = res.data;
          
          $("#detail-device").html(`
          <div class="card-title-group align-start pb-3 g-2">
              <div class="card-title card-title-sm">
                  <h6 class="title">Detail data perangkat IEMS</h6>
              </div>
          </div>
          
          <div class="nk-block">
              <div class="profile-ud-list">
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Nama Perangkat</span>
                          <span class="profile-ud-value">
                              ${device.device_name}<br>
                              <small>${device.device_shortname}</small>
                          </span>
                      </div>
                  </div>
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">ID Perangkat</span>
                          <span class="profile-ud-value">${device.device_token}</span>
                      </div>
                  </div>
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Kode Aktifasi</span>
                          <span class="profile-ud-value">20201008201010-8871</span>
                      </div>
                  </div>
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Jenis Perangkat</span>
                          <span class="profile-ud-value">${getStatus(device.device_type)}</span>
                      </div>
                  </div>
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Status</span>
                          <span class="profile-ud-value">${getStatus(device.device_status)}</span>
                      </div>
                  </div>
              </div><!-- .profile-ud-list -->
          </div>

          <hr>

          <div class="card-title-group align-start pb-3 g-2">
              <div class="card-title card-title-sm">
                  <h6 class="title">Additional Information</h6>
                  <p>Informasi tambahan perangkat IEMS</p>
              </div>
              <div class="card-tools">
                  <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Additional Information"></em>
              </div>
          </div>

          <div class="nk-block">
              <div class="profile-ud-list">
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Tanggal Aktif</span>
                          <span class="profile-ud-value">${(device.device_status != "active") ? "-" : global.date(device.created_at)}</span>
                      </div>
                  </div>
                  <div class="profile-ud-item">
                      <div class="profile-ud wider">
                          <span class="profile-ud-label">Tanggal Ubah</span>
                          <span class="profile-ud-value">${(device.device_status != "active") ? "-" : global.date(device.updated_at)}</span>
                      </div>
                  </div>
              </div><!-- .profile-ud-list -->
          </div>
          `);

            return;
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function getStatus(status) {
  var newStatus = `<span class="badge badge-pill badge-success">Aktif</span>`
  switch (status) {
      case "kwh-1-phase": newStatus = `<span class="badge badge-pill badge-outline-warning">KWH 1 Fasa</span>`;
      break;
      case "kwh-3-phase": newStatus = `<span class="badge badge-pill badge-outline-info">KWH 3 Fasa</span>`;
      break;
      case "pcb": newStatus = `<span class="badge badge-pill badge-outline-primary">PCB</span>`;
      break;
      case "sensors": newStatus = `<span class="badge badge-pill badge-outline-success">Sensor</span>`;
      break;
      case "slca": newStatus = `<span class="badge badge-pill badge-outline-secondary">SLCA</span>`;
      break;
      case "active": newStatus = `<span class="badge badge-pill badge-success">Aktif</span>`
      break;
      case "not active yet": newStatus = `<span class="badge badge-pill badge-secondary">Belum Diaktifasi</span>`
      break;
      case "blocked": newStatus = `<span class="badge badge-pill badge-danger">Terblokir</span>`
      break;
  }
  return newStatus;
}

function getPull()
{
  $("#list-data-device tbody").html(`
    <tr>
      <td colspan="4">
        <center>
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
        </center>
      </td>
    </tr>
    `);

    global.getRAW(global.base_url + "/services/devices/pull?device_token=" + device_token + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {

          dataDevices = res.data;
          metaDataDevices = res.meta;
          
          $("#list-data-device tbody").html(`${dataDevices.map(function(item, position) {
            return `
            <tr>
                <th scope="row">${(position + 1)}</th>
                <td>${item.mode}</td>
                <td>${item.date}</td>
                <td>${item.time}</td>
            </tr>
            `;
          }).join('')}`);

          if (dataDevices.length == 0) {
          $("#list-data-device tbody").html(`
          <tr>
            <td colspan="4">
              <center>
                Belum ada data perangkat
              </center>
            </td>
          </tr>
          `);
          }

            return;
        } else {
          $("#list-data-device tbody").html(`
            <tr>
              <td colspan="4">
                <center>
                  ${res.message}
                </center>
              </td>
            </tr>
            `);
        }
    });
}