"use strict";

var devices = [];
var device = null;

!function (NioApp, $) {
  "use strict";

  filterDevices();

  $("#btn-device-add").on("click",function() {
    $("#btn-device-add").attr("disabled", true);
    $("#btn-device-add").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    setTimeout(() => {
        location.assign(global.base_url + "/views/devicesAdd");
    }, 1000);
  });

  $("#btn-device-edit").on("click",function() {
    $("#btn-device-edit").attr("disabled", true);
    $("#btn-device-edit").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doEditDevice();
  });
}(NioApp, jQuery);

function gotoDetailDevice(position) { 
    device = devices[position];
    location.assign(global.base_url + "/views/devicesDetail?tokenize=" + global.csrf_token + "&uid=" + device.device_token + "&uname=" + device.device_name + "&utype=" + device.device_type);
}

function setSelectedEdit(position) {
    device = devices[position];
    $("#modal-edit-device-name").text(device.device_name)
    $("#device-edit-name").val(device.device_name)
}

function doEditDevice() {
  var id = device.id;
  var device_name = $("#device-edit-name").val()

  var raw = global.raw({
    device_name: device_name
  })

  var formData = new FormData();
  formData.append("raw", raw);

  // Do Login
  global.postRAW(formData, 
    global.base_url + "/services/devices/update/"+id+"?tokenize=" + global.csrf_token, function(res) {
      
      $("#btn-device-edit").removeAttr("disabled");
      $("#btn-device-edit").html(`
        <span>Simpan</span>
        <em class="icon ni ni-arrow-right"></em>
      `);

      if (res.code == 200) {

        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
        $("#modal-edit-device-form").trigger("reset");
        $("#modal-edit-device").modal('toggle');
        filterDevices();

        return
      }
      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function filterDevices()
{
    $("#list-devices tbody").html(`
        <tr>
            <td scope="col" colspan="6">
                <center>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span> Loading... </span>
                </center>
            </td>
        </tr>
    `);

    global.getRAW(global.base_url + "/services/devices/filter?&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
            devices = res.data;
            $("#list-devices tbody").html(`${devices.map(function(item, position) {
                var type = getStatus(item.device_type);
                var status = getStatus(item.device_status);
                var action = `
                <button class="btn btn-icon btn-sm btn-primary mb-1"
                onclick="gotoDetailDevice(${position})">
                <em class="icon ni ni-eye"></em></button>
                <button class="btn btn-icon btn-sm btn-warning mb-1"
                data-toggle="modal"
                data-target="#modal-edit-device"
                onclick="setSelectedEdit(${position})">
                <em class="icon ni ni-edit"></em></button>
                `;

                return `
                <tr>
                  <th scope="row">${(position + 1)}</th>
                  <td>
                      ${item.device_name}<br>
                      <small>${item.device_shortname}</small>
                  </td>
                  <td>
                      <small>${item.device_token}</small>
                  </td>
                  <td>${type}</td>
                  <td>${status}</td>
                  <td>${(item.device_status == "not active yet") ? "-" : global.date(item.updated_at)}</td>
                  <td>${action}</td>
              </tr>
                `;
            }).join('')}`);
            return;
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function getStatus(status) {
    var newStatus = `<span class="badge badge-pill badge-success">Aktif</span>`
    switch (status) {
        case "kwh-1-phase": newStatus = `<span class="badge badge-pill badge-outline-warning">KWH 1 Fasa</span>`;
        break;
        case "kwh-3-phase": newStatus = `<span class="badge badge-pill badge-outline-info">KWH 3 Fasa</span>`;
        break;
        case "pcb": newStatus = `<span class="badge badge-pill badge-outline-primary">PCB</span>`;
        break;
        case "sensors": newStatus = `<span class="badge badge-pill badge-outline-success">Sensor</span>`;
        break;
        case "slca": newStatus = `<span class="badge badge-pill badge-outline-secondary">SLCA</span>`;
        break;
        case "active": newStatus = `<span class="badge badge-pill badge-success">Aktif</span>`
        break;
        case "not active yet": newStatus = `<span class="badge badge-pill badge-secondary">Belum Diaktifasi</span>`
        break;
        case "blocked": newStatus = `<span class="badge badge-pill badge-danger">Terblokir</span>`
        break;
    }
    return newStatus;
}