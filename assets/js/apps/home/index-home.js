"use strict";

var devices = [];
var maps;

!function (NioApp, $) {
  "use strict";

  $("#btn-monitor-device").on("click",function() {
    $("#btn-monitor-device").attr("disabled", true);
    $("#btn-monitor-device").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    location.assign(global.base_url + "/views/devices");
  });

  loadMetaTotal()

}(NioApp, jQuery);

function convertToType(type) {
    switch (type) {
        case "kwh-1-phase": return "KWH 1 Fasa";
        case "kwh-3-phase": return "KWH 3 Fasa";
        case "pcb": return "MCB";
        case "slca": return "Single Load Ampermeter";
        default: return "Sensor";
    }
}

function loadMetaTotal() {

  $("#meta-total-data").html(`
  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
  <span> Loading... </span>
  `);

  global.getRAW(global.base_url + "/services/meta/home?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
          $("#meta-total-data").html(`
          <div class="col-md-4">
            <div class="card card-bordered card-full">
                <div class="card-inner">
                    <div class="card-title-group align-start mb-0">
                        <div class="card-title">
                            <h6 class="subtitle">Total Perangkat</h6>
                        </div>
                        <div class="card-tools">
                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                        </div>
                    </div>
                    <div class="card-amount">
                        <span class="amount">${res.data.devices.total}
                        </span>
                        <span class="change up text-danger">
                            <em class="icon ni ni-archive"></em> Perangkat
                        </span>
                    </div>
                    <div class="invest-data">
                        <div class="invest-data-amount g-2">
                            <div class="invest-data-history">
                                <div class="title">Bulan Ini</div>
                                <div class="amount">${res.data.devices.month}</div>
                            </div>
                            <div class="invest-data-history">
                                <div class="title">Hari Ini</div>
                                <div class="amount">${res.data.devices.day}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .card -->
        </div>

        <div class="col-md-4">
            <div class="card card-bordered card-full">
                <div class="card-inner">
                    <div class="card-title-group align-start mb-0">
                        <div class="card-title">
                            <h6 class="subtitle">Total Pengguna</h6>
                        </div>
                        <div class="card-tools">
                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Pengguna"></em>
                        </div>
                    </div>
                    <div class="card-amount">
                        <span class="amount">${res.data.users.total}
                        </span>
                        <span class="change up text-danger">
                            <em class="icon ni ni-users"></em> Pengguna
                        </span>
                    </div>
                    <div class="invest-data">
                        <div class="invest-data-amount g-2">
                            <div class="invest-data-history">
                                <div class="title">Bulan Ini</div>
                                <div class="amount">${res.data.users.month}</div>
                            </div>
                            <div class="invest-data-history">
                                <div class="title">Hari Ini</div>
                                <div class="amount">${res.data.users.day}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .card -->
        </div>

        <div class="col-md-4">
            <div class="card card-bordered card-full">
                <div class="card-inner">
                    <div class="card-title-group align-start mb-0">
                        <div class="card-title">
                            <h6 class="subtitle">Tiket Yang Masuk</h6>
                        </div>
                        <div class="card-tools">
                            <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Perangkat Yang Aktif"></em>
                        </div>
                    </div>
                    <div class="card-amount">
                        <span class="amount">${res.data.ticketing.total}
                        </span>
                        <span class="change up text-danger">
                            <em class="icon ni ni-archive"></em> Perangkat
                        </span>
                    </div>
                    <div class="invest-data">
                        <div class="invest-data-amount g-2">
                            <div class="invest-data-history">
                                <div class="title">Bulan Ini</div>
                                <div class="amount">${res.data.ticketing.month}</div>
                            </div>
                            <div class="invest-data-history">
                                <div class="title">Hari Ini</div>
                                <div class="amount">${res.data.ticketing.day}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .card -->
        </div>
          `);

          myDevices();
            
          return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function myDevices() {
    global.getRAW(global.base_url + "/services/devices/filter?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
            devices = res.data;

            setupKWH();
            setupMCB();
            setupSLCA();
            setupSensor();
            
          return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function getLastDataDevice(device_token, callBack) {
    global.getRAW(global.base_url + "/services/devices/last?device_token=" + device_token + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
            callBack(res.data)
            
          return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function setOnOffDevice(device_token, newValue, callBack) {
    var raw = global.raw({
        "mode": newValue
    });
    var formData = new FormData();
    formData.append("raw", raw);

    global.postRAW(formData, global.base_url + "/services/devices/push?device_token=" + device_token + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
            callBack(res.data)
            
          return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function setupMCB() {
    var tempDevice = ``;
    $("#list-device-mcb").html(tempDevice);

    var deviceMCB = [];
    devices.forEach(function(item) { 
        if (item.device_type == "pcb") deviceMCB.push(item) 
    })

    if (deviceMCB.length == 0) $("#list-device-mcb").html(`
        <center>Belum ada perangkat</center>
    `);
    else deviceMCB.forEach(function(item) {
        getLastDataDevice(item.device_token, function(data) {
            if (data == null) return;
            var target = global.base_url + "/views/devicesDetail?tokenize=" + global.csrf_token + "&uid=" + item.device_token + "&uname=" + item.device_name + "&utype=" + item.device_type;
            $("#list-device-mcb").append(`
            <div class="accordion-item">
                <a href="#" class="accordion-head" data-toggle="collapse" data-target="#accordion-item-${item.device_token}">
                    <h6 class="title">MCB - ${item.device_name}</h6>
                    <span class="accordion-icon"></span>
                </a>
                <div class="accordion-body collapse" id="accordion-item-${item.device_token}" data-parent="#list-power-status">
                    <div class="accordion-inner">
                        <p>
                            <a href="${target}" class="btn btn-primary">Selengkapnya</a>
                        </p>
                        <div class="row">
                            <div class="col-md-4 my-2">
                            <div class="card card-bordered card-full"
                            style="cursor: pointer"
                            onclick="setOnOffMCB('${data.mode}', '${item.device_token}')">
                                <div class="card-inner">
                                <div class="card-title-group align-start mb-0">
                                    <div class="card-title">
                                        <h6 class="subtitle">Mode</h6>
                                    </div>
                                    <div class="card-tools">
                                        <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                                    </div>
                                </div>
                                <div class="card-amount">
                                    <span class="amount"> ${data.mode}
                                    </span>
                                </div>
                                </div>
                            </div><!-- .card -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `);
        });
    });
}

function setOnOffMCB(currentMode, device_token) {
    setOnOffDevice(device_token, currentMode == 'OFF' ? 'ON' : 'OFF', function(data) {
        NioApp.Toast("Berhasil mengubah status perangkat", 'success', {position: 'bottom-right'});
        setupMCB();
    });
}

function setupSLCA() {
    var tempDevice = ``;
    $("#list-device-single-load").html(tempDevice);

    var deviceSLCA = [];
    devices.forEach(function(item) { 
        if (item.device_type == "slca") deviceSLCA.push(item) 
    })

    if (deviceSLCA.length == 0) $("#list-device-single-load").html(`
        <center>Belum ada perangkat</center>
    `);
    else deviceSLCA.forEach(function(item) {
        getLastDataDevice(item.device_token, function(data) {
            console.log(data);
            if (data == null) return;
            var target = global.base_url + "/views/devicesDetail?tokenize=" + global.csrf_token + "&uid=" + item.device_token + "&uname=" + item.device_name + "&utype=" + item.device_type;
            $("#list-device-single-load").append(`
            <div class="accordion-item">
                <a href="#" class="accordion-head" data-toggle="collapse" data-target="#accordion-item-${item.device_token}">
                    <h6 class="title">Single Load Ampermeter - ${item.device_name}</h6>
                    <span class="accordion-icon"></span>
                </a>
                <div class="accordion-body collapse" id="accordion-item-${item.device_token}" data-parent="#list-power-status">
                    <div class="accordion-inner">
                        <p>
                            <a href="${target}" class="btn btn-primary">Selengkapnya</a>
                        </p>
                        <div class="row">
                            <div class="col-md-4 my-2">
                            <div class="card card-bordered card-full"
                            style="cursor: pointer"
                            onclick="setOnOffSLCA('${data.status.mode}', '${item.device_token}')">
                                <div class="card-inner">
                                <div class="card-title-group align-start mb-0">
                                    <div class="card-title">
                                        <h6 class="subtitle">Mode</h6>
                                    </div>
                                    <div class="card-tools">
                                        <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                                    </div>
                                </div>
                                <div class="card-amount">
                                    <span class="amount"> ${data.status.mode}
                                    </span>
                                </div>
                                </div>
                            </div><!-- .card -->
                            </div>

                            <div class="col-md-4 my-2">
                            <div class="card card-bordered card-full">
                                <div class="card-inner">
                                <div class="card-title-group align-start mb-0">
                                    <div class="card-title">
                                        <h6 class="subtitle">Current</h6>
                                    </div>
                                    <div class="card-tools">
                                        <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                                    </div>
                                </div>
                                <div class="card-amount">
                                    <span class="amount"> ${(data == null) ? 0 : Number(data.iac).toFixed(2)}
                                    </span>
                                    <span class="change up text-danger">
                                        <em class="icon ni ni-archive"></em> A
                                    </span>
                                </div>
                                </div>
                            </div><!-- .card -->
                            </div>

                            <div class="col-md-4 my-2">
                            <div class="card card-bordered card-full">
                                <div class="card-inner">
                                <div class="card-title-group align-start mb-0">
                                    <div class="card-title">
                                        <h6 class="subtitle">Power</h6>
                                    </div>
                                    <div class="card-tools">
                                        <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                                    </div>
                                </div>
                                <div class="card-amount">
                                    <span class="amount"> ${(data == null) ? 0 : Number(data.power).toFixed(2)}
                                    </span>
                                    <span class="change up text-danger">
                                        <em class="icon ni ni-archive"></em> kWatt
                                    </span>
                                </div>
                                </div>
                            </div><!-- .card -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `);
        });
    });
}

function setOnOffSLCA(currentMode, device_token) {
    setOnOffDevice(device_token, currentMode == 'OFF' ? 'ON' : 'OFF', function(data) {
        NioApp.Toast("Berhasil mengubah status perangkat", 'success', {position: 'bottom-right'});
        setupSLCA();
    });
}

function setupSensor() {
    var tempDevice = ``;
    $("#list-device-sensor").html(tempDevice);

    var deviceSensor = [];
    devices.forEach(function(item) { 
        if (item.device_type == "sensors") deviceSensor.push(item) 
    })

    if (deviceSensor.length == 0) $("#list-device-sensor").html(`
        <center>Belum ada perangkat</center>
    `);
    else deviceSensor.forEach(function(item) {
        getLastDataDevice(item.device_token, function(data) {
            var target = global.base_url + "/views/devicesDetail?tokenize=" + global.csrf_token + "&uid=" + item.device_token + "&uname=" + item.device_name + "&utype=" + item.device_type;
            $("#list-device-sensor").html(`
            <div class="accordion-item">
                <a href="#" class="accordion-head" data-toggle="collapse" data-target="#accordion-item-${item.device_token}">
                    <h6 class="title">Sensor - ${item.device_name}</h6>
                    <span class="accordion-icon"></span>
                </a>
                <div class="accordion-body collapse" id="accordion-item-${item.device_token}" data-parent="#list-power-status">
                    <div class="accordion-inner">
                        <p>
                            <a href="${target}" class="btn btn-primary">Selengkapnya</a>
                        </p>
                        <div class="row">
                            <div class="col-md-4 my-2">
                            <div class="card card-bordered card-full">
                                <div class="card-inner">
                                <div class="card-title-group align-start mb-0">
                                    <div class="card-title">
                                        <h6 class="subtitle">PIR</h6>
                                    </div>
                                    <div class="card-tools">
                                        <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                                    </div>
                                </div>
                                <div class="card-amount">
                                    <span class="amount"> ${(data == null) ? 0 : Number(data.pir).toFixed(2)}
                                    </span>
                                </div>
                                </div>
                            </div><!-- .card -->
                            </div>

                            <div class="col-md-4 my-2">
                            <div class="card card-bordered card-full">
                                <div class="card-inner">
                                <div class="card-title-group align-start mb-0">
                                    <div class="card-title">
                                        <h6 class="subtitle">TEMPERATURE</h6>
                                    </div>
                                    <div class="card-tools">
                                        <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                                    </div>
                                </div>
                                <div class="card-amount">
                                    <span class="amount"> ${(data == null) ? 0 : Number(data.temp).toFixed(2)}
                                    </span>
                                    <span class="change up text-danger">
                                        <em class="icon ni ni-archive"></em> °C
                                    </span>
                                </div>
                                </div>
                            </div><!-- .card -->
                            </div>

                            <div class="col-md-4 my-2">
                            <div class="card card-bordered card-full">
                                <div class="card-inner">
                                <div class="card-title-group align-start mb-0">
                                    <div class="card-title">
                                        <h6 class="subtitle">HUMADITY</h6>
                                    </div>
                                    <div class="card-tools">
                                        <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                                    </div>
                                </div>
                                <div class="card-amount">
                                    <span class="amount"> ${(data == null) ? 0 : Number(data.hum).toFixed(2)}
                                    </span>
                                    <span class="change up text-danger">
                                        <em class="icon ni ni-archive"></em> RH
                                    </span>
                                </div>
                                </div>
                            </div><!-- .card -->
                            </div>

                            <div class="col-md-4 my-2">
                            <div class="card card-bordered card-full">
                                <div class="card-inner">
                                <div class="card-title-group align-start mb-0">
                                    <div class="card-title">
                                        <h6 class="subtitle">LUXURY</h6>
                                    </div>
                                    <div class="card-tools">
                                        <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                                    </div>
                                </div>
                                <div class="card-amount">
                                    <span class="amount"> ${(data == null) ? 0 : Number(data.lux).toFixed(2)}
                                    </span>
                                </div>
                                </div>
                            </div><!-- .card -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `);
        });
    });
}

function setupKWH() {
    var tempDevice = ``;
    $("#list-power-status").html(tempDevice);

    var deviceKWH = [];
    devices.forEach(function(item) { 
        if (item.device_type == "kwh-1-phase") deviceKWH.push(item) 
        else if (item.device_type == "kwh-3-phase") deviceKWH.push(item)
    })

    $("#calculate-total-device").text(deviceKWH.length)
    
    deviceKWH.forEach(function(item) {
        if (item.device_type == "kwh-1-phase") {
            getLastDataDevice(item.device_token, function(data) {
                tempDevice = setUIKWH1(item, data);
                $("#list-power-status").append(tempDevice);
            });
        } else if (item.device_type == "kwh-3-phase") {
            getLastDataDevice(item.device_token, function(data) {
                tempDevice = setUIKWH3(item, data);
                $("#list-power-status").append(tempDevice);
            });
        }
    })
}

function setupMyDevice() {
    
}

function setUIKWH1(item, data) {
    var target = global.base_url + "/views/devicesDetail?tokenize=" + global.csrf_token + "&uid=" + item.device_token + "&uname=" + item.device_name + "&utype=" + item.device_type;
    return `
    <div class="accordion-item">
        <a href="#" class="accordion-head" data-toggle="collapse" data-target="#accordion-item-${item.device_token}">
            <h6 class="title">Power Status - ${item.device_name}</h6>
            <span class="accordion-icon"></span>
        </a>
        <div class="accordion-body collapse" id="accordion-item-${item.device_token}" data-parent="#list-power-status">
            <div class="accordion-inner">
                <p>
                    <a href="${target}" class="btn btn-primary">Selengkapnya</a>
                </p>
                <div class="row">
                    <div class="col-md-4 my-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Voltage</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : Number(data.v).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> V
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>
                    <div class="col-md-4 my-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Current</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : Number(data.i).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> A
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>
                    <div class="col-md-4 my-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Frequency</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : Number(data.f)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> Hz
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>

                    <div class="col-md-4 mt-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Power</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : Number(data.pa)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> Watt
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>

                    <div class="col-md-4 mt-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Power Factor</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : Number(data.pf)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> 
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>

                    <div class="col-md-4 mt-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Energy</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : Number(data.kwh)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> kWh
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>

                </div><!-- .device-status -->
            </div>
        </div>
    </div>
    `;
}

function setUIKWH3(item, data) {
    var target = global.base_url + "/views/devicesDetail?tokenize=" + global.csrf_token + "&uid=" + item.device_token + "&uname=" + item.device_name + "&utype=" + item.device_type;
    return `
    <div class="accordion-item">
        <a href="#" class="accordion-head" data-toggle="collapse" data-target="#accordion-item-${item.device_token}">
            <h6 class="title">Power Status - ${item.device_name}</h6>
            <span class="accordion-icon"></span>
        </a>
        <div class="accordion-body collapse" id="accordion-item-${item.device_token}" data-parent="#list-power-status">
            <div class="accordion-inner">
                <p>
                    <a href="${target}" class="btn btn-primary">Selengkapnya</a>
                </p>
                <div class="row">

                    <div class="col-md-4 my-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Voltage (R)</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : (Number(data.va)).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> V
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>
                    <div class="col-md-4 my-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Voltage (S)</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : (Number(data.vb)).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> V
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>
                    <div class="col-md-4 my-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Voltage (T)</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : (Number(data.vc)).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> V
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>
                    <div class="col-md-4 my-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Current (R)</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 :(Number(data.ia)).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> A
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>
                    <div class="col-md-4 my-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Current (S)</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 :(Number(data.ib)).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> A
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>
                    <div class="col-md-4 my-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Current (T)</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 :(Number(data.ic)).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> A
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>

                    <div class="col-md-4 mt-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Power Factor (R)</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : (Number(data.pfa)).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> 
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>
                    <div class="col-md-4 mt-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Power Factor (S)</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : (Number(data.pfb)).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> 
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>
                    <div class="col-md-4 mt-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Power Factor (T)</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : (Number(data.pfc)).toFixed(2)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> 
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>

                    <div class="col-md-4 my-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Frequency</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : Number(data.freq)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> Hz
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>

                    <div class="col-md-4 mt-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Power</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : Number(data.pt)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> kWatt
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>

                    <div class="col-md-4 mt-2">
                    <div class="card card-bordered card-full">
                        <div class="card-inner">
                        <div class="card-title-group align-start mb-0">
                            <div class="card-title">
                                <h6 class="subtitle">Energy</h6>
                            </div>
                            <div class="card-tools">
                                <em class="card-hint icon ni ni-help-fill" data-toggle="tooltip" data-placement="left" title="" data-original-title="Total Perangkat"></em>
                            </div>
                        </div>
                        <div class="card-amount">
                            <span class="amount"> ${(data == null) ? 0 : Number(data.ep)}
                            </span>
                            <span class="change up text-danger">
                                <em class="icon ni ni-archive"></em> kWh
                            </span>
                        </div>
                        </div>
                    </div><!-- .card -->
                    </div>

                </div><!-- .device-status -->
            </div>
        </div>
    </div>
    `;
}