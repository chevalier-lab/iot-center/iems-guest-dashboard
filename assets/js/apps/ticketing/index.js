"use strict";

var tickets = []
var selectedType = "";

!function (NioApp, $) {
  "use strict";

  // Load Filter Users
  filterTickets(selectedType); // Load All

  $("#btn-ticketing-add").on("click",function() {
    $("#btn-ticketing-add").attr("disabled", true);
    $("#btn-ticketing-add").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doCreateTicket();
  });

}(NioApp, jQuery);

function gotoDetailUser(position) {
    var user = users[position];
    location.assign(global.base_url + "/views/usersDetail?tokenize=" + global.csrf_token + "&uid=" + user.phone_number + "&uname=" + user.full_name);
}

function doCreateTicket() {
    var title = $("#ticket-new-title").val();
    var description = $("#ticket-new-description").val();

  var raw = global.raw({
    title: title,
    description: description
  })

  var formData = new FormData();
  formData.append("raw", raw);

  // Do Login
  global.postRAW(formData, 
    global.base_url + "/services/ticketing/create?tokenize=" + global.csrf_token, function(res) {
      
      $("#btn-ticketing-add").removeAttr("disabled");
      $("#btn-ticketing-add").html(`
        <span>Simpan</span>
        <em class="icon ni ni-arrow-right"></em>
      `);

      if (res.code == 200) {

        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
        $("#modal-ticketing-user-form").trigger("reset");
        $("#modal-ticketing-user").modal('toggle');

        filterTickets(selectedType)

        return
      }
      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function filterTickets(type)
{
    $("#ticket-list tbody").html(`
        <tr>
            <td scope="col" colspan="6">
                <center>
                <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span> Loading... </span>
                </center>
            </td>
        </tr>
    `);

    selectedType = type;

    global.getRAW(global.base_url + "/services/ticketing/filter?type=" + type + "&tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {
            tickets = res.data;
            $("#ticket-list tbody").html(`${tickets.map(function(item, position) {
                var status = getStatus(item.status);
                return `
                <tr>
                    <th scope="row">${(position + 1)}</th>
                    <td>${item.full_name}</td>
                    <td>${item.phone_number}</td>
                    <td>${item.title}</td>
                    <td>${status}</td>
                    <td>${global.date(item.updated_at)}</td>
                    <td>
                        <button class="btn btn-icon btn-sm btn-primary"
                        onclick="showDetailTicket(${position})"
                        data-toggle="modal"
                        data-target="#modal-ticketing-user-detail">
                        <em class="icon ni ni-eye"></em></button>
                    </td>
                </tr>
                `;
            }).join('')}`);
            return;
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function showDetailTicket(position) {
  var ticket = tickets[position];
  var status = getStatus(ticket.status);
  $("#ticketing-user-detail-title").text(ticket.title)
  $("#ticketing-user-detail-full_name").text(ticket.full_name)
  $("#ticketing-user-detail-phone_number").html(ticket.phone_number)
  $("#ticketing-user-detail-status").html(status)
  $("#ticketing-user-detail-date").text(global.date(ticket.updated_at))
  $("#ticketing-user-detail-description").html(ticket.description)
}

function getStatus(status) {
    var newStatus = `<span class="badge badge-pill badge-info">Baru</span>`
    switch (status) {
        case "new": newStatus = `<span class="badge badge-pill badge-info">Baru</span>`;
        break;
        case "process": newStatus = `<span class="badge badge-pill badge-warning">Diproses</span>`;
        break;
        case "finish": newStatus = `<span class="badge badge-pill badge-success">Selesai</span>`;
        break;
        case "rejected": newStatus = `<span class="badge badge-pill badge-danger">Ditolak</span>`;
        break;
    }
    return newStatus;
}