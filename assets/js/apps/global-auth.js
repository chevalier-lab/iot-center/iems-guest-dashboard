"use strict";

var profile;

!function (NioApp, $) {
  "use strict";

  // Load Profile
  loadProfile()
}(NioApp, jQuery);

function loadProfile() {
    // Setup Before
    $("#sidebar-profile").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);
    $("#topbar-profile").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);
    $("#welcome-profile").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    // Process
    global.getRAW(global.base_url + "/services/general/profile?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {

            profile = res.data;

            // If Banned
            if (res.data.status == "banned") {
                NioApp.Toast("Maaf akun anda sudah dimatikan/banned", 'error', {position: 'bottom-right'});
                return
            }

            // Setup Sidebar Profile
            $("#sidebar-profile").html(`
                <strong>${res.data.full_name}</strong>
                <div><small>${res.data.phone_number}</small></div>
            `);

            // Setup Topbar Profile
            $("#topbar-profile").html(`
            <div class="user-avatar sm">
                <em class="icon ni ni-user-alt"></em>
            </div>
            <div class="user-info d-none d-md-block">
                <div class="user-status">${res.data.type}</div>
                <div class="user-name dropdown-indicator">${res.data.full_name}</div>
            </div>
            `);

            // Setup Topbar Profile
            $("#topbar-card-profile").html(`
            <div class="user-avatar">
                <span>${res.data.full_name.substring(0, 2)}</span>
            </div>
            <div class="user-info">
                <span class="lead-text">${res.data.full_name}</span>
                <span class="sub-text">${res.data.phone_number}</span>
            </div>
            `);

            // Setup Welcome Profile
            $("#welcome-profile").html(`${res.data.full_name}`);
            return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}