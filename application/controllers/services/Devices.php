<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Devices extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(IEMS_PUBLIC_AUTH)) {
            $this->auth = $this->session->get_session(IEMS_PUBLIC_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    // Load Filter Devices
    public function filter()
    {   
        $this->request->header(array("Authorization: " . $this->auth->token));
        
        if ($this->auth->type == "user") {
            $res = $this->request->get("/services/user/manage/devices");
        } else {
            $res = $this->request->get("/services/guest/manage/devices");
        }

        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function userOrGuestDevice()
    {
        $phone_number = $this->input->get("phone_number", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/user/manage/deviceGuest?phone_number=$phone_number");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function info()
    {
        $device_token = $this->input->get("device_token", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/public/devices/info?device_token=$device_token");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function pull()
    {
        $page = $this->input->get("page", TRUE) ?: 0;
        $device_token = $this->input->get("device_token", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/public/devices/pull?device_token=$device_token&page=$page");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function last()
    {
        $device_token = $this->input->get("device_token", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/public/devices/last?device_token=$device_token");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    // Activate Device
    public function activate()
    {
        $raw = $this->input->post_get("raw") ?: "";
        
        if (!empty($raw)) {
            $this->request->header(array(
                "Content-type: application/json",
                "Authorization: " . $this->auth->token
            ));
            $res = $this->request->post($raw, "/services/operator/manage/devices/activate");
            $res = json_decode($res, true);

            if ($res['success']) {
                die(json_encode($res));
            } else die(json_encode(
                array(
                    "code" => 500,
                    "message" => $res["message"]
                )
            ));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Parameter tidak benar"
            )
        ));
    }

    // Create Device
    public function update($id)
    {
        $raw = $this->input->post_get("raw") ?: "";
        
        if (!empty($raw)) {
            $this->request->header(array(
                "Content-type: application/json",
                "Authorization: " . $this->auth->token
            ));
            $res = $this->request->post($raw, "/services/user/manage/devices/update/$id");
            $res = json_decode($res, true);

            if ($res['success']) {
                die(json_encode($res));
            } else die(json_encode(
                array(
                    "code" => 500,
                    "message" => $res["message"]
                )
            ));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Parameter tidak benar"
            )
        ));
    }

    // Push Data
    public function push()
    {
        $raw = $this->input->post_get("raw") ?: "";
        $device_token = $this->input->post_get("device_token") ?: "";
        
        if (!empty($raw)) {
            $this->request->header(array(
                "Content-type: application/json",
                "Authorization: " . $this->auth->token
            ));
            $res = $this->request->post($raw, "/public/devices/push?device_token=$device_token");
            $res = json_decode($res, true);

            if ($res['success']) {
                die(json_encode($res));
            } else die(json_encode(
                array(
                    "code" => 500,
                    "message" => $res["message"]
                )
            ));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Parameter tidak benar"
            )
        ));
    }

}
