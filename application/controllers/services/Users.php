<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(IEMS_PUBLIC_AUTH)) {
            $this->auth = $this->session->get_session(IEMS_PUBLIC_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }

    // Load Profile
    public function profile()
    {
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/profile");
        $res = json_decode($res, true);

        if ($res['code'] == 200) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 501,
                "message" => $res["message"]
            )
        ));
    }
    
    // Load Filter Users
    public function filter()
    {   
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/user/manage/guest");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    // Detail User
    public function detail()
    {
        $phone_number = $this->input->get("phone_number") ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/user/manage/guest/detail?phone_number=$phone_number");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function invited()
    {
        $raw = $this->input->post_get("raw") ?: "";
        if (!empty($raw)) {
            $this->request->header(array(
                "Content-type: application/json",
                "Authorization: " . $this->auth->token
            ));
            $res = $this->request->post($raw, "/services/user/manage/guest/invited");
            $res = json_decode($res, true);

            if ($res['success']) {
                die(json_encode($res));
            } else die(json_encode(
                array(
                    "code" => 500,
                    "message" => $res["message"]
                )
            ));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Parameter tidak benar"
            )
        ));
    }

    // Do Update
    public function update()
    {
        $raw = $this->input->post_get("raw") ?: "";
        
        if (!empty($raw)) {
            $this->request->header(array(
                "Content-type: application/json",
                "Authorization: " . $this->auth->token
            ));
            $res = $this->request->post($raw, "/services/administrator/manage/users/update");
            $res = json_decode($res, true);

            if ($res['success']) {
                die(json_encode($res));
            } else die(json_encode(
                array(
                    "code" => 500,
                    "message" => $res["message"]
                )
            ));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Parameter tidak benar"
            )
        ));
    }

    // Info
    public function info()
    {
        $phone_number = $this->input->get("phone_number", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/administrator/manage/users/info?phone_number=$phone_number");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

}
