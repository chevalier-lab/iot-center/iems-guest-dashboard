<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Meta extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(IEMS_PUBLIC_AUTH)) {
            $this->auth = $this->session->get_session(IEMS_PUBLIC_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    // Load Meta Home
    public function home()
    {   
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/meta/totalDataForUser");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function users()
    {   
        $this->request->header(array("Authorization: " . $this->auth->token));

        $res = $this->request->get("/services/meta/totalDataForUser");

        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function tickets()
    {   
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/meta/totalDataTicket");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function devices()
    {   
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/meta/totalDataDevice");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

}
