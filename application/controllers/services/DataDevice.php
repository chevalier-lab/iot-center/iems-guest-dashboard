<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DataDevice extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(IEMS_PUBLIC_AUTH)) {
            $this->auth = $this->session->get_session(IEMS_PUBLIC_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    // Load Filter Devices
    public function day()
    {   
        $device_token = $this->input->get("device_token", TRUE) ?: "";
        $type = $this->input->get("type", TRUE) ?: "KWH1";
        $param = $this->input->get("param", TRUE) ?: "kwh";
        $date = $this->input->get("date", TRUE) ?: date("Y-m-d");
        $time = $this->input->get("time", TRUE) ?: "";

        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/public/data-devices/$type/day?device_token=$device_token&param=$param&date=$date&time=$time");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function month()
    {
        $device_token = $this->input->get("device_token", TRUE) ?: "";
        $type = $this->input->get("type", TRUE) ?: "KWH1";
        $param = $this->input->get("param", TRUE) ?: "kwh";
        $date = $this->input->get("date", TRUE) ?: date("Y-m-d");

        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/public/data-devices/$type/month?device_token=$device_token&param=$param&date=$date");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function year()
    {
        $device_token = $this->input->get("device_token", TRUE) ?: "";
        $type = $this->input->get("type", TRUE) ?: "KWH1";
        $param = $this->input->get("param", TRUE) ?: "kwh";
        $date = $this->input->get("date", TRUE) ?: date("Y-m-d");

        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/public/data-devices/$type/year?device_token=$device_token&param=$param&date=$date");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

}
