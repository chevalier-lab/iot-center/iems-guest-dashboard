<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ticketing extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(IEMS_PUBLIC_AUTH)) {
            $this->auth = $this->session->get_session(IEMS_PUBLIC_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    // Load Filter Ticketing
    public function filter()
    {   
        $type = $this->input->get("type", TRUE) ?: "";
        $this->request->header(array("Authorization: " . $this->auth->token));
        $res = $this->request->get("/services/ticketing/my?type=$type");
        $res = json_decode($res, true);

        if ($res['success']) {
            die(json_encode($res));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => $res["message"]
            )
        ));
    }

    public function create()
    {
        $raw = $this->input->post_get("raw") ?: "";
        if (!empty($raw)) {
            $this->request->header(array(
                "Content-type: application/json",
                "Authorization: " . $this->auth->token
            ));
            $res = $this->request->post($raw, "/services/ticketing/create");
            $res = json_decode($res, true);

            if ($res['success']) {
                die(json_encode($res));
            } else die(json_encode(
                array(
                    "code" => 500,
                    "message" => $res["message"]
                )
            ));
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Parameter tidak benar"
            )
        ));
    }

}
