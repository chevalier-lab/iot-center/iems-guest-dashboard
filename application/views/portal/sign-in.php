<?php
    $jsDir = base_url().'/assets/js/apps/';

    $this->load->view('templates/portal', array(
        "title" => "Portal Masuk - Dashboard IEMS",
        "additional" => "",
        "jsLibrary" => "
            <script src='".$jsDir."portal/sign-in.js'></script>
        ",
        "content" => '
            <h4 class="title mb-4">Masuk ke Dashboard</h4>
            <form action="#" class="gy-3">

                <center>
                    <img src="https://pasca.isi.ac.id/wp-content/uploads/2018/05/lpdp-large-512x500.png"
                        style="max-width: 120px" />
                </center>

                <div class="row g-3 align-center">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="form-label text-right" 
                            style="display: block"
                            for="usernameOrPhoneNumber">Username / Nomor Telepon</label>
                            <span class="form-note text-right"
                            style="display: block">Masukkan username / nomor telepon yang sudah terdaftar.</span>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="form-group">
                            <div class="form-control-wrap">
                                <input type="text" class="form-control" id="usernameOrPhoneNumber"
                                placeholder="Username / Nomor Telepon">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row g-3 align-center">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="form-label text-right" 
                            style="display: block"
                            for="password">Password</label>
                            <span class="form-note text-right"
                            style="display: block">Masukkan password yang sudah terdaftar.</span>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="form-group">
                            <div class="form-control-wrap">
                                <div class="form-icon form-icon-right"
                                onclick="showTogglePassword()">
                                    <em class="icon ni ni-eye"></em>
                                </div>

                                <input type="password" class="form-control" id="password"
                                placeholder="Password">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="g-3 clearfix">
                    <div class="form-group">
                        <button
                        type="button" 
                        style="float:right"
                        id="btn-sign-in"
                        class="btn btn-wider btn-primary">
                            <span>Masuk</span>
                            <em class="icon ni ni-arrow-right"></em>
                        </button>
                    </div>
                </div>

            </form>
        '
    ));
?>