<!-- Modal Content Code -->
<div class="modal fade" tabindex="-1" id="modal-ticketing-user-detail">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                <em class="icon ni ni-cross"></em>
            </a>
            <div class="modal-header">
                <h5 class="modal-title">Detail Tiket</h5>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <table>
                            <tr>
                                <th>Judul Tiket</th>
                                <td>:</td>
                                <td id="ticketing-user-detail-title"></td>
                            </tr>
                            <tr>
                                <th>Nama Pengaju</th>
                                <td>:</td>
                                <td id="ticketing-user-detail-full_name"></td>
                            </tr>
                            <tr>
                                <th>Nomor Telepon</th>
                                <td>:</td>
                                <td id="ticketing-user-detail-phone_number"></td>
                            </tr>
                            <tr>
                                <th>Status Tiket</th>
                                <td>:</td>
                                <td id="ticketing-user-detail-status"></td>
                            </tr>
                            <tr>
                                <th>Tanggal Perubahan</th>
                                <td>:</td>
                                <td id="ticketing-user-detail-date"></td>
                            </tr>
                            <tr>
                                <th>Deskripsi Tiket</th>
                                <td>:</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3" id="ticketing-user-detail-description"></td>
                            </tr>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>