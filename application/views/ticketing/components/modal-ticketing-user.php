<!-- Modal Content Code -->
<div class="modal fade" tabindex="-1" id="modal-ticketing-user">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                <em class="icon ni ni-cross"></em>
            </a>
            <div class="modal-header">
                <h5 class="modal-title">Buat Tiket Baru?</h5>
            </div>
            <div class="modal-body">

                <form class="row" id="modal-ticketing-user-form">
                    <div class="col-sm-12 mb-2 if-user">
                        <div class="form-group">
                            <label class="form-label" for="ticket-new-title">Judul Tiket</label>
                            <div class="form-control-wrap">
                                <input type="text" class="form-control" 
                                id="ticket-new-title" placeholder="Judul Tiket">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 mb-2 if-user">
                        <div class="form-group">
                            <label class="form-label" for="ticket-new-description">Deskripsi Permasalahan</label>
                            <div class="form-control-wrap">
                                <textarea class="form-control" 
                                id="ticket-new-description" placeholder="Deskripsi Permasalahan"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-light">

                <div class="clearfix">
                    <div class="form-group">
                        <button
                        type="button" 
                        style="float:right"
                        id="btn-ticketing-add"
                        class="btn btn-wider btn-success">
                            <span>Simpan</span>
                            <em class="icon ni ni-arrow-right"></em>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>