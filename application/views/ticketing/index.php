<?php
    $jsDir = base_url().'/assets/js/apps/';
    $today = date('Y-m-d H:i:s');
    $compDir = 'ticketing/components/';

    $this->load->view('templates/dashboard', array(
        "title" => "Kelola Tiket - Dashboard IEMS",
        "additional" => "",
        "jsLibrary" => "
            <script src='".$jsDir."ticketing/index.js'></script>
        ",
        "components" => array(
            $compDir . "modal-ticketing-user",
            $compDir . "modal-ticketing-user-detail"
        ),
        "content" => '
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">Kelola Tiket</h3>
                    <div class="nk-block-des text-soft">
                        <p>Selamat Datang <strong id="welcome-profile"></strong></p>
                    </div>
                </div><!-- .nk-block-head-content -->
                <div class="nk-block-head-content">
                    <div class="toggle-wrap nk-block-tools-toggle">
                        <div class="toggle-expand-content" data-content="pageMenu">
                            <ul class="nk-block-tools g-3">
                                <li class="nk-block-tools-opt"><button 
                                href="javascript:void(0)" 
                                id="btn-ticket-add"
                                data-toggle="modal"
                                data-target="#modal-ticketing-user"
                                class="btn btn-primary"><em class="icon ni ni-plus"></em><span>Buat Tiket</span></button></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-between -->
        </div>

        <div class="nk-block">
            <div class="row g-gs">

                <div class="col-xxl-8">
                    <div class="card card-bordered card-preview">
                        <div class="card-inner">
                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Daftar Tiket</h6>
                                    <p>Daftar data tiket IEMS sampai <strong>'.$today.'</strong></p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Daftar Pengguna"></em>
                                </div>
                            </div>

                            <ul class="nav nav-tabs mt-n3">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterTickets(``)">Semua</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterTickets(`new`)">Baru</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterTickets(`process`)">Proses</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterTickets(`finish`)">Selesai</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="javascript:void(0)"
                                    onclick="filterTickets(`rejected`)">Ditolak</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active">

                                <div class="form-group">
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-right">
                                            <em class="icon ni ni-search"></em>
                                        </div>
                                        <input type="search" class="form-control"
                                        placeholder="Cari berdasarkan judul, nama lengkap dan nomor telepon">
                                    </div>
                                </div>
                                    
                                <div class="table-responsive">
                                    <table class="table" id="ticket-list">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Nama Lengkap</th>
                                                <th scope="col">Nomor Telepon</th>
                                                <th scope="col">Judul</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Tanggal</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- .row -->
        </div>
        '
    ));
?>