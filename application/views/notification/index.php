<?php
    $jsDir = base_url().'/assets/js/apps/';
    $today = date('Y-m-d H:i:s');

    $this->load->view('templates/dashboard', array(
        "title" => "Notifikasi Perangkat - Dashboard IEMS",
        "additional" => "",
        "jsLibrary" => "
            <script src='".$jsDir."general.js'></script>
            <script src='".$jsDir."devices/index.js'></script>
        ",
        "content" => '
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">Notifikasi Perangkat</h3>
                    <div class="nk-block-des text-soft">
                        <p>Selamat Datang <strong>Andy Maulana Yusuf</strong></p>
                    </div>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-between -->
        </div>

        <div class="nk-block">
            <div class="row g-gs">

                <div class="col-xxl-8">
                    <div class="card card-bordered card-preview">
                        <div class="card-inner">
                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Daftar Perangkat</h6>
                                    <p>Daftar data perangkat IEMS sampai <strong>'.$today.'</strong></p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Daftar Perangkat"></em>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-right">
                                        <em class="icon ni ni-search"></em>
                                    </div>
                                    <input type="search" class="form-control"
                                    placeholder="Cari berdasarkan nama perangkat, dan ID perangkat">
                                </div>
                            </div>
                                
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nama Perangkat</th>
                                            <th scope="col">Notifikasi</th>
                                            <th scope="col">Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <th scope="row">1</th>
                                            <td>
                                                KWH 1 Fasa - 20201008201010<br>
                                                <small>kwh1f-20201008201010</small>
                                            </td>
                                            <td>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta illum repudiandae eligendi magnam voluptatum, aut optio. Voluptate provident atque odit aliquam sit, mollitia laudantium ullam et deleniti unde inventore consequuntur.
                                            </td>
                                            <td>01 Januari 2021 20:20:20</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">2</th>
                                            <td>
                                                KWH 1 Fasa - 20201008201010<br>
                                                <small>kwh1f-20201008201010</small>
                                            </td>
                                            <td>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta illum repudiandae eligendi magnam voluptatum, aut optio. Voluptate provident atque odit aliquam sit, mollitia laudantium ullam et deleniti unde inventore consequuntur.
                                            </td>
                                            <td>01 Januari 2021 20:20:20</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">3</th>
                                            <td>
                                                KWH 1 Fasa - 20201008201010<br>
                                                <small>kwh1f-20201008201010</small>
                                            </td>
                                            <td>
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta illum repudiandae eligendi magnam voluptatum, aut optio. Voluptate provident atque odit aliquam sit, mollitia laudantium ullam et deleniti unde inventore consequuntur.
                                            </td>
                                            <td>01 Januari 2021 20:20:20</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div><!-- .row -->
        </div>

        <!-- Modal Content Code -->
        <div class="modal fade" tabindex="-1" id="modalDelete">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <em class="icon ni ni-cross"></em>
                    </a>
                    <div class="modal-header">
                        <h5 class="modal-title">Hapus Perangkat?</h5>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda yakin ingin menghapus <strong>KWH 1 Fasa - 20201008201010</strong>?</p>
                    </div>
                    <div class="modal-footer bg-light">

                        <div class="clearfix">
                            <div class="form-group">
                                <button
                                type="button" 
                                style="float:right"
                                id="btn-device-delete"
                                class="btn btn-wider btn-danger">
                                    <span>Hapus</span>
                                    <em class="icon ni ni-arrow-right"></em>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Content Code -->
        <div class="modal fade" tabindex="-1" id="modalBanned">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <em class="icon ni ni-cross"></em>
                    </a>
                    <div class="modal-header">
                        <h5 class="modal-title">Larang Perangkat?</h5>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda yakin ingin melarang <strong>KWH 1 Fasa - 20201008201010</strong>?</p>
                    </div>
                    <div class="modal-footer bg-light">

                        <div class="clearfix">
                            <div class="form-group">
                                <button
                                type="button" 
                                style="float:right"
                                id="btn-device-banned"
                                class="btn btn-wider btn-danger">
                                    <span>Larang</span>
                                    <em class="icon ni ni-arrow-right"></em>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Content Code -->
        <div class="modal fade" tabindex="-1" id="modalActivate">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <em class="icon ni ni-cross"></em>
                    </a>
                    <div class="modal-header">
                        <h5 class="modal-title">Aktifasi Perangkat?</h5>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda yakin ingin mengaktifkan <strong>KWH 1 Fasa - 20201008201010</strong>?</p>
                    </div>
                    <div class="modal-footer bg-light">

                        <div class="clearfix">
                            <div class="form-group">
                                <button
                                type="button" 
                                style="float:right"
                                id="btn-device-activate"
                                class="btn btn-wider btn-success">
                                    <span>Aktifkan</span>
                                    <em class="icon ni ni-arrow-right"></em>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        '
    ));
?>