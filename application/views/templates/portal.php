<!DOCTYPE html>
<html lang="zxx" class="js">

<?php
    $this->load->view('templates/meta/head', array(
        "title" => isset($title) ? $title : "Portal",
        "additional" => isset($additional) ? $additional : ""
    ));
?>

<body class="nk-body bg-primary npc-general ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap @@wrapClass">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="wide-md text-center m-auto h-100">
                        <div class="logo-link mb-5 mt-4">
                            <h4 class="title mt-4 text-white">IEMS Dashboard</h4>
                            <p class="text-white">Intelligent Energy Management System.</p>
                        </div>
                        <div class="row g-gs" style="max-width: 1024px">

                            <div class="col-lg-12">
                                <div class="card card-bordered text-soft py-4">
                                    <div class="card-inner">

                                        <?= isset($content) ? $content : ""; ?>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

    <?php
        $this->load->view('templates/meta/footer', array(
            "additional" => isset($jsLibrary) ? $jsLibrary : ""
        ));
    ?>

</html>