<!-- sidebar @s -->
<div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
    <div class="nk-sidebar-element nk-sidebar-head">
        <div class="logo-light logo-img"
            style="width: 64px; height: 64px;
            border-radius: 8px;
            background-image: url('<?= base_url('/'); ?>images/avatar/a-sm.jpg');
            background-size: cover;
            background-position: center;"></div>
        <div id="sidebar-profile">
        </div>
        <div class="nk-menu-trigger mr-n2">
            <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
        </div>
    </div><!-- .nk-sidebar-element -->
    <div class="nk-sidebar-element">
        <div class="nk-sidebar-content">
            <div class="nk-sidebar-menu" data-simplebar>
                <ul class="nk-menu">

                    <?php
                        if (isset($menu)) {
                            foreach ($menu as $menuItem) {
                                ?>
                                <li class="nk-menu-heading">
                                    <h6 class="overline-title text-primary-alt">
                                    <?= $menuItem["header"]; ?>
                                    </h6>
                                </li>
                                <?php
                                foreach ($menuItem["items"] as $item) {
                                    ?>
                                    <!-- .nk-menu-item -->
                                    <li class="nk-menu-item">
                                        <a href="<?= $item['uri'] ?>" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-<?= $item['icon']; ?>"></em></span>
                                            <span class="nk-menu-text"><?= $item['label']; ?></span>
                                        </a>
                                    </li>
                                    <?php
                                }
                            }
                        }
                    ?>

                </ul><!-- .nk-menu -->
            </div><!-- .nk-sidebar-menu -->
        </div><!-- .nk-sidebar-content -->
    </div><!-- .nk-sidebar-element -->
</div>
<!-- sidebar @e -->