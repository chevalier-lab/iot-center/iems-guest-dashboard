<!-- Modal Content Code -->
<div class="modal fade" tabindex="-1" id="modal-invited-user">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                <em class="icon ni ni-cross"></em>
            </a>
            <div class="modal-header">
                <h5 class="modal-title">Undang Pengguna?</h5>
            </div>
            <div class="modal-body">

                <form class="row" id="modal-invited-user-form">
                    <div class="col-sm-12 mb-2 if-user">
                        <div class="form-group">
                            <label class="form-label" for="user-new-phone_number">Nomor Telepon Pengguna</label>
                            <div class="form-control-wrap">
                                <input type="number" class="form-control" 
                                id="user-new-phone_number" placeholder="Nomor Telepon Pengguna">
                            </div>
                        </div>
                    </div>
                </form>

                <p>Apakah anda yakin ingin mengundang pengguna?</p>
            </div>
            <div class="modal-footer bg-light">

                <div class="clearfix">
                    <div class="form-group">
                        <button
                        type="button" 
                        style="float:right"
                        id="btn-user-invited"
                        class="btn btn-wider btn-success">
                            <span>Undang</span>
                            <em class="icon ni ni-arrow-right"></em>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>