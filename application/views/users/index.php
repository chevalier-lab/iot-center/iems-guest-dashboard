<?php
    $jsDir = base_url().'/assets/js/apps/';
    $compDir = 'users/components/';
    $today = date('Y-m-d H:i:s');

    $this->load->view('templates/dashboard', array(
        "title" => "Kelola Pengguna - Dashboard IEMS",
        "additional" => "",
        "jsLibrary" => "
            <script src='".$jsDir."users/index.js'></script>
        ",
        "components" => array(
            $compDir . "modal-invited-user"
        ),
        "content" => '
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">Kelola Pengguna</h3>
                    <div class="nk-block-des text-soft">
                        <p>Selamat Datang <strong id="welcome-profile"></strong></p>
                    </div>
                </div><!-- .nk-block-head-content -->
                <div class="nk-block-head-content">
                    <div class="toggle-wrap nk-block-tools-toggle">
                        <div class="toggle-expand-content" data-content="pageMenu">
                            <ul class="nk-block-tools g-3">
                                <li class="nk-block-tools-opt"><button 
                                href="javascript:void(0)" 
                                id="btn-user-add"
                                data-toggle="modal"
                                data-target="#modal-invited-user"
                                class="btn btn-primary"><em class="icon ni ni-plus"></em><span>Undang Pengguna Baru</span></button></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-between -->
        </div>

        <div class="nk-block">
            <div class="row g-gs">

                <div class="col-xxl-8">
                    <div class="card card-bordered card-preview">
                        <div class="card-inner">
                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Daftar Pengguna</h6>
                                    <p>Daftar data pengguna IEMS sampai <strong>'.$today.'</strong></p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Daftar Pengguna"></em>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active">

                                <div class="form-group">
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-right">
                                            <em class="icon ni ni-search"></em>
                                        </div>
                                        <input type="search" class="form-control"
                                        placeholder="Cari berdasarkan nama lengkap, dan nomor telepon">
                                    </div>
                                </div>
                                    
                                <div class="table-responsive">
                                    <table class="table" id="user-list">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Nama Lengkap</th>
                                                <th scope="col">Nomor Telepon</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Tanggal Aktif</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- .row -->
        </div>
        '
    ));
?>