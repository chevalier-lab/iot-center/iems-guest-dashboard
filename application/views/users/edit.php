<?php
    $jsDir = base_url().'/assets/js/apps/';

    $this->load->view('templates/dashboard', array(
        "title" => "Ubah Pengguna - Dashboard IEMS",
        "additional" => "",
        "jsLibrary" => "
            <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js'></script>
            <script src='".$jsDir."users/edit.js'></script>
        ",
        "content" => '
        <input type="hidden" id="selected-user-phone_number" value="'.$udata["phone_number"].'">
        
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">Ubah Pengguna</span></h3>
                    <div class="nk-block-des text-soft">
                        <p>Selamat Datang <strong>Andy Maulana Yusuf</strong></p>
                    </div>
                </div><!-- .nk-block-head-content -->
                <div class="nk-block-head-content">
                    <div class="toggle-wrap nk-block-tools-toggle">
                        <div class="toggle-expand-content" data-content="pageMenu">
                            <ul class="nk-block-tools g-3">
                                <li class="nk-block-tools-opt"><button 
                                href="javascript:void(0)" 
                                id="btn-user-back"
                                class="btn btn-primary"><em class="icon ni ni-arrow-left"></em><span>Kembali</span></button></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-between -->
        </div>

        <div class="nk-block">
            <div class="row g-gs">
                
                <div class="col-xxl-3">
                    <div class="card card-bordered h-100">
                        <div class="card-inner">

                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Form Ubah Pengguna</h6>
                                    <p>Form ubah pengguna IEMS</p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Form Ubah Pengguna"></em>
                                </div>
                            </div>
                            <form class="row" id="user-new-form">

                                <div class="col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label class="form-label" for="user-new-full_name">Nama Lengkap</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control" 
                                            id="user-new-full_name" placeholder="Nama Lengkap">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label class="form-label" for="user-new-email">E-Mail</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control" 
                                            id="user-new-email" placeholder="E-Mail">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label class="form-label" for="user-new-password">Password</label>
                                        <div class="form-control-wrap">
                                            <input type="password" class="form-control" 
                                            id="user-new-password" placeholder="Password">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label class="form-label" for="user-new-phone_number">Nomor Telepon</label>
                                        <div class="form-control-wrap">
                                            <input type="tel" class="form-control" 
                                            disabled
                                            id="user-new-phone_number" placeholder="Nomor Telepon">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mb-2">
                                    <div class="form-group">
                                        <label class="form-label" for="user-new-type">Jenis Pengguna</label>
                                        <div class="form-control-wrap">
                                            <select id="user-new-type" class="form-control"
                                            onchange="checkIsUser(this.value)" disabled>
                                                <option value="guest">Biasa</option>
                                                <option value="user">Pengguna (Aktif)</option>
                                                <option value="operator">Operator</option>
                                                <option value="customer service">Customer Service</option>
                                                <option value="administrator">Administrator</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mb-2">
                                </div>

                                <div class="col-sm-6 mb-2 if-user">
                                    <div class="form-group">
                                        <label class="form-label" for="user-new-ssid_name">Nama SSID</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control" 
                                            id="user-new-ssid_name" placeholder="Nama SSID">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 mb-2 if-user">
                                    <div class="form-group">
                                        <label class="form-label" for="user-new-ssid_password">SSID Password</label>
                                        <div class="form-control-wrap">
                                            <input type="password" class="form-control" 
                                            id="user-new-ssid_password" placeholder="SSID Password">
                                        </div>
                                    </div>
                                </div>

                            </form>

                            <div class="mt-4 clearfix">
                                <div class="form-group">
                                    <button
                                    type="button" 
                                    style="float:right"
                                    id="btn-user-new"
                                    class="btn btn-wider btn-primary">
                                        <span>Simpan</span>
                                        <em class="icon ni ni-arrow-right"></em>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div><!-- .card -->
                </div><!-- .col -->

            </div><!-- .row -->
        </div>
        '
    ));
?>