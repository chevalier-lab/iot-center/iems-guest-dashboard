<?php
    $jsDir = base_url().'/assets/js/apps/';
    $jsDirExample = base_url().'/assets/js/';
    $today = date('Y-m-d H:i:s');

    $this->load->view('templates/dashboard', array(
        "title" => "Beranda - Dashboard IEMS",
        "additional" => "
        <link rel='stylesheet' href='https://unpkg.com/leaflet@1.7.1/dist/leaflet.css'
        integrity='sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=='
        crossorigin=''/>
        ",
        "jsLibrary" => "
            <script src='https://unpkg.com/leaflet@1.7.1/dist/leaflet.js'
            integrity='sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=='
            crossorigin=''></script>

            <script src='".$jsDir."home/index.js'></script>
            <script src='".$jsDirExample."example-chart.js?ver=1.4.0'></script>
            <script src='".$jsDirExample."charts/gd-analytics.js?ver=1.4.0'></script>
        ",
        "content" => '
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">Beranda</h3>
                    <div class="nk-block-des text-soft">
                        <p>Selamat Datang <strong id="welcome-profile">Andy Maulana Yusuf</strong></p>
                    </div>
                </div><!-- .nk-block-head-content -->
                <div class="nk-block-head-content">
                    <div class="toggle-wrap nk-block-tools-toggle">
                        <div class="toggle-expand-content" data-content="pageMenu">
                            <ul class="nk-block-tools g-3">
                                <li class="nk-block-tools-opt"><button 
                                href="javascript:void(0)" 
                                id="btn-monitor-device"
                                class="btn btn-primary"><em class="icon ni ni-monitor"></em><span>Pantau Perangkat</span></button></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-between -->
        </div>

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-between-md g-2">
                    <div class="nk-block-head-content">
                        <h5 class="nk-block-title title">Sebaran Perangkat</h5>
                    </div>
                </div>
            </div><!-- .nk-block-head -->

            <div class="row gy-gs">
                <div class="col-12">
                    <div class="card card-bordered h-100">
                        <div class="card-inner">
                            <div id="maps" style="height: 400px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="nk-block">
            <div class="row gy-gs">
                <div class="col-lg-5 col-xl-4">
                    <div class="nk-block">
                        <div class="nk-block-head-xs">
                            <div class="nk-block-head-content">
                                <h5 class="nk-block-title title">Konsumsi kWh Total</h5>
                            </div>
                        </div><!-- .nk-block-head -->
                        <div class="nk-block">
                            <div class="card card-bordered text-light is-dark h-100">
                                <div class="card-inner">
                                    <div class="nk-wg7">
                                        <div class="nk-wg7-stats">
                                            <div class="nk-wg7-title">Total Tagihan (IDR)</div>
                                            <div class="number amount" id="calculate-total-bills"
                                            style="font-size: 24px !important;">-</div>
                                        </div>
                                        <div class="nk-wg7-stats-group">
                                            <div class="nk-wg7-stats w-50">
                                                <div class="nk-wg7-title">Perangkat</div>
                                                <div class="number" id="calculate-total-device">-</div>
                                            </div>
                                            <div class="nk-wg7-stats w-50">
                                                <div class="nk-wg7-title">Energi (kWh)</div>
                                                <div class="number" id="calculate-total-kWh">-</div>
                                            </div>
                                        </div>
                                        <div class="nk-wg7-foot">
                                            <span class="nk-wg7-note">Pertanggal <span>'.$today.'</span></span>
                                        </div>
                                    </div><!-- .nk-wg7 -->
                                </div><!-- .card-inner -->
                            </div><!-- .card -->
                        </div><!-- .nk-block -->
                    </div><!-- .nk-block -->
                </div><!-- .col -->
                <div class="col-lg-7 col-xl-8">
                    <div class="nk-block">
                                
                        <div class="nk-block-head-xs">
                            <div class="nk-block-between-md g-2">
                                <div class="nk-block-head-content">
                                    <h5 class="nk-block-title title">Konsumsi kWh setiap Group</h5>
                                </div>
                                <div class="nk-block-head-content">
                                    <a href="html/crypto/wallets.html" class="link link-primary">Selengkapnya</a>
                                </div>
                            </div>
                        </div><!-- .nk-block-head -->
                        
                        <div class="card card-bordered h-100">
                            <div class="card-inner">
                                <div class="row g-2" id="calculate-groups">
                                </div><!-- .row -->
                            </div>
                        </div>
                            
                    </div><!-- .nk-block -->
                    
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .nk-block -->

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Group Perangkat</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="row g-gs" id="list-device-group">
            </div>
        </div>

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Perangkat kWh</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="card card-bordered card-preview">
                <div class="card-inner">
                    <div id="list-power-status" class="accordion accordion-s3">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Perangkat MCB</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="card card-bordered card-preview">
                <div class="card-inner">
                    <div id="list-device-mcb" class="accordion accordion-s3">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Perangkat Single Load Ampermeter</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="card card-bordered card-preview">
                <div class="card-inner">
                    <div id="list-device-single-load" class="accordion accordion-s3">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Perangkat Sensor</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="card card-bordered card-preview">
                <div class="card-inner">
                    <div id="list-device-sensor" class="accordion accordion-s3">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Perangkat AC</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="card card-bordered card-preview">
                <div class="card-inner">
                    <div id="list-device-ac" class="accordion accordion-s3">
                        
                    </div>
                </div>
            </div>
        </div>
        '
    ));
?>
