<?php
    $jsDir = base_url().'/assets/js/apps/';
    $jsDirExample = base_url().'/assets/js/';
    $today = date('Y-m-d H:i:s');

    $this->load->view('templates/dashboard', array(
        "title" => "Beranda - Dashboard IEMS",
        "additional" => "
        <link rel='stylesheet' href='https://unpkg.com/leaflet@1.7.1/dist/leaflet.css'
        integrity='sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=='
        crossorigin=''/>
        ",
        "jsLibrary" => "
            <script src='https://unpkg.com/leaflet@1.7.1/dist/leaflet.js'
            integrity='sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=='
            crossorigin=''></script>

            <script src='".$jsDir."home/index-home.js'></script>
            <script src='".$jsDirExample."example-chart.js?ver=1.4.0'></script>
            <script src='".$jsDirExample."charts/gd-analytics.js?ver=1.4.0'></script>
        ",
        "content" => '
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">Beranda</h3>
                    <div class="nk-block-des text-soft">
                        <p>Selamat Datang <strong id="welcome-profile">Andy Maulana Yusuf</strong></p>
                    </div>
                </div><!-- .nk-block-head-content -->
                <div class="nk-block-head-content">
                    <div class="toggle-wrap nk-block-tools-toggle">
                        <div class="toggle-expand-content" data-content="pageMenu">
                            <ul class="nk-block-tools g-3">
                                <li class="nk-block-tools-opt"><button 
                                href="javascript:void(0)" 
                                id="btn-monitor-device"
                                class="btn btn-primary"><em class="icon ni ni-monitor"></em><span>Pantau Perangkat</span></button></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-between -->
        </div>

        <div class="nk-block">
            <div class="row gy-gs" id="meta-total-data">
            </div><!-- .row -->
        </div><!-- .nk-block -->

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Perangkat kWh</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="card card-bordered card-preview">
                <div class="card-inner">
                    <div id="list-power-status" class="accordion accordion-s3">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Perangkat MCB</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="card card-bordered card-preview">
                <div class="card-inner">
                    <div id="list-device-mcb" class="accordion accordion-s3">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Perangkat Single Load Ampermeter</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="card card-bordered card-preview">
                <div class="card-inner">
                    <div id="list-device-single-load" class="accordion accordion-s3">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Perangkat Sensor</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="card card-bordered card-preview">
                <div class="card-inner">
                    <div id="list-device-sensor" class="accordion accordion-s3">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="nk-block">
            <div class="nk-block-head-xs">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title title">Daftar Perangkat AC</h5>
                </div>
            </div><!-- .nk-block-head -->

            <div class="card card-bordered card-preview">
                <div class="card-inner">
                    <div id="list-device-ac" class="accordion accordion-s3">
                        
                    </div>
                </div>
            </div>
        </div>
        '
    ));
?>
