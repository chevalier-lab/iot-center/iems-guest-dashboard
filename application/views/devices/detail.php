<?php
    switch ($udata["device_type"]) {
        case "kwh-1-phase": $this->load->view("devices/detail/kwh-1");
        break;
        case "kwh-3-phase": $this->load->view("devices/detail/kwh-3");
        break;
        case "sensors": $this->load->view("devices/detail/sensor");
        break;
        case "pcb": $this->load->view("devices/detail/pcb");
        break;
        default: $this->load->view("devices/detail/slca");
        break;
    }
?>