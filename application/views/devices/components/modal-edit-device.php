<!-- Modal Content Code -->
<div class="modal fade" tabindex="-1" id="modal-edit-device">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                <em class="icon ni ni-cross"></em>
            </a>
            <div class="modal-header">
                <h5 class="modal-title">Ubah Nama Perangkat</h5>
            </div>
            <div class="modal-body">
                <form class="row" id="modal-edit-device-form">
                    <div class="col-sm-6 mb-2 if-user">
                        <div class="form-group">
                            <label class="form-label" for="device-edit-name">Nama Perangkat</label>
                            <div class="form-control-wrap">
                                <input type="text" class="form-control" 
                                id="device-edit-name" placeholder="Nama Perangkat">
                            </div>
                        </div>
                    </div>
                </form>

                <p>Apakah anda yakin ingin mengubah nama perangkat <strong id="modal-edit-device-name"></strong>?</p>
            </div>
            <div class="modal-footer bg-light">

                <div class="clearfix">
                    <div class="form-group">
                        <button
                        type="button" 
                        style="float:right"
                        id="btn-device-edit"
                        class="btn btn-wider btn-success">
                            <span>Simpan</span>
                            <em class="icon ni ni-arrow-right"></em>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>