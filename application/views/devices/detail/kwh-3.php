<?php
    $jsDir = base_url().'/assets/js/apps/';
    $jsDirExample = base_url().'/assets/js/';
    $today = date('Y-m-d H:i:s');

    $this->load->view('templates/dashboard', array(
        "title" => "Detail Perangkat - Dashboard IEMS",
        "additional" => "",
        "jsLibrary" => "
            <script src='https://cdn.plot.ly/plotly-latest.min.js'></script>
            <script src='".$jsDir."devices/detail/kwh-3.js'></script>
        ",
        "content" => '
        <input type="hidden" id="selected-device-device_token" value="'.$udata["device_token"].'">

        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">Detail Perangkat</h3>
                    <div class="nk-block-des text-soft">
                    <p>Selamat Datang <strong id="welcome-profile"></strong></p>
                    </div>
                </div><!-- .nk-block-head-content -->
                <div class="nk-block-head-content">
                    <div class="toggle-wrap nk-block-tools-toggle">
                        <div class="toggle-expand-content" data-content="pageMenu">
                            <ul class="nk-block-tools g-3">
                                <li class="nk-block-tools-opt"><button 
                                href="javascript:void(0)" 
                                id="btn-device-back"
                                class="btn btn-primary"><em class="icon ni ni-arrow-left"></em><span>Kembali</span></button></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .nk-block-head-content -->
            </div><!-- .nk-block-between -->
        </div>

        <div class="nk-block">
            <div class="row g-gs">
                
                <div class="col-xxl-3">
                    <div class="card card-bordered h-100">
                        <div class="card-inner" id="detail-device">

                            

                        </div>
                    </div><!-- .card -->
                </div><!-- .col -->

                <div class="col-xxl-3">
                    <div class="card card-bordered">
                        <div class="card-inner">

                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Grafik Data Perangkat</h6>
                                    <p>Grafik riwayat data perangkat</p>
                                </div>
                                <div class="card-tools">
                                    <em class="card-hint icon ni ni-help" data-toggle="tooltip" data-placement="left" title="" data-original-title="Grafik Data Perangkat"></em>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <div class="form-control-wrap">
                                        <select id="device-plot-type" class="form-control"
                                        onchange="setType(this.value)">
                                            <option value="0" selected>Realtime</option>
                                            <option value="1">Akumulasi 1 Bulan</option>
                                            <option value="2">Data Bulanan</option>
                                            <option value="3">Tanggal Tertentu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-right">
                                            <em class="icon ni ni-calendar"></em>
                                        </div>
                                        <input type="text" class="form-control date-picker-alt"
                                        id="device-plot-date"
                                        data-date-format="yyyy-mm-dd"
                                        placeholder="Cari berdasarkan tanggal" disabled>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-right">
                                            <em class="icon ni ni-history"></em>
                                        </div>
                                        <input type="text" class="form-control time-picker"
                                        id="device-plot-time"
                                        placeholder="Cari berdasarkan waktu" disabled>
                                    </div>
                                </div>
                                <div class="form-group col-md-2">
                                    <button class="btn btn-primary btn-block"
                                    id="doPlotFilter">Terapkan</button>
                                </div>
                                
                                <div class="form-group col-md-5">
                                    <div class="form-control-wrap">
                                        <select id="device-plot-param" class="form-control"
                                        onchange="setSelectedParam(this.value)">
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-5">
                                    <div class="form-control-wrap">
                                        <select id="device-plot-display" class="form-control">
                                            <option value="sum" selected>Nilai Total</option>
                                            <option value="avg">Nilai Rata-rata</option>
                                            <option value="last">Nilai Terakhir</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div id="display-plot"></div>

                        </div>
                    </div><!-- .card -->
                </div><!-- .col -->

                <div class="col-xxl-8">
                    <div class="card card-bordered card-preview">
                        <div class="card-inner">
                            
                            <div class="card-title-group align-start pb-3 g-2">
                                <div class="card-title card-title-sm">
                                    <h6 class="title">Daftar Perangkat</h6>
                                    <p>Daftar data perangkat IEMS sampai <strong>'.$today.'</strong></p>
                                </div>
                                <div class="card-tools">
                                    <button class="btn btn-primary" type="button"
                                    data-toggle="modal" data-target="#modalGrafik">
                                        <em class="icon ni ni-trend-up"></em>
                                        <span>Lihat Grafik</span>
                                    </button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-4">
                                    <div class="form-control-wrap">
                                        <select id="device-new-type" class="form-control">
                                            <option value="">Realtime</option>
                                            <option value="">Minggu Ini</option>
                                            <option value="">Bulan Ini</option>
                                            <option value="">3 Bulan Terakhir</option>
                                            <option value="">Tahun Ini</option>
                                            <option value="">Semuanya</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-right">
                                            <em class="icon ni ni-calendar"></em>
                                        </div>
                                        <input type="date" class="form-control date-picker"
                                        placeholder="Cari berdasarkan tanggal">
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <div class="form-control-wrap">
                                        <div class="form-icon form-icon-right">
                                            <em class="icon ni ni-history"></em>
                                        </div>
                                        <input type="time" class="form-control time-picker"
                                        placeholder="Cari berdasarkan waktu">
                                    </div>
                                </div>
                            </div>

                            <div class="card-title-group align-start g-2">
                                <div class="card-title card-title-sm">
                                    <p>Cari berdasarkan <strong>Tanggal</strong> dan <strong>Waktu</strong></p>
                                </div>
                            </div>

                            <div class="table-responsive">
                            <table class="table" id="list-data-device">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Va</th>
                                            <th scope="col">Vb</th>
                                            <th scope="col">Vc</th>
                                            <th scope="col">Vab</th>
                                            <th scope="col">Vbc</th>
                                            <th scope="col">Vca</th>
                                            <th scope="col">Ia</th>
                                            <th scope="col">Ib</th>
                                            <th scope="col">Ic</th>
                                            <th scope="col">Pa</th>
                                            <th scope="col">Pb</th>
                                            <th scope="col">Pc</th>
                                            <th scope="col">Pt</th>
                                            <th scope="col">Qa</th>
                                            <th scope="col">Qb</th>
                                            <th scope="col">Qc</th>
                                            <th scope="col">Qt</th>
                                            <th scope="col">Sa</th>
                                            <th scope="col">Sb</th>
                                            <th scope="col">Sc</th>
                                            <th scope="col">St</th>
                                            <th scope="col">PFa</th>
                                            <th scope="col">PFb</th>
                                            <th scope="col">PFc</th>
                                            <th scope="col">Freq</th>
                                            <th scope="col">EP</th>
                                            <th scope="col">EQ</th>
                                            <th scope="col">Tanggal</th>
                                            <th scope="col">Waktu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div><!-- .row -->
        </div>
        '
    ));
?>